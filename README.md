# Project Meta-Verse

## Project Overview

* Meta-Verse is a platform for collaborative and secure machine learning in a distributed environment.
* This branch implements the FedPCL algorithm.

## Folder Structure
The codebase is organized into several folders:
* Codes: Contains the core implementation of the project.
1. **Client_Server**:
This directory contains Python (.py) files responsible for implementing client and server classes components.

2. **Data**:
This folder hosts 4 files represent the idea of holding reading modifying and processing the datasets, the folders we have are:
- **DaiseeDataInduction.py**
- **CreditScoringDataInduction.py**
- **MnistDataInduction.py**
- **InductionForData.py**
- **PreProcessing.py**
- **ImagePreProcessing.py**
Each of the above files contains a class that does the work represented by its name. 
For the classes within (DaiseeDataInduction.py, CreditScoringDataInduction.py, MnistDataInduction.py) all of them inherits from the class within (InductionForData.py)
The PreProcessing.py file contains functions of feature selection that were used mainly with CreditScoring data and a class called CustomLabelBinirizer this class is used to convert the label from regular from to one hot form.
Lastly the ImagePreProcessing.py file contains a class that is responsible for some famous preprocessing methods on images datasets

3. **Differential_Privacy**:
The files in this directory implement the concept of Differential Privacy (DP) for client models' weights, ensuring privacy-preserving machine learning.

4. **Inter_Clustering_Grouping**:
This section showcases a custom clustering algorithm designed to accommodate various types of data, fostering effective inter-clustering and grouping.

5. **Models**:
Here, you'll find 9 .py files.
The following files represents the creation of OSELM model and FAOSELM:
* **BaseRandomLayer.py**
* **GRBFRandomLayer.py**
* **MLPRandomLayer.py**
* **RandomLayer.py**
* **RBFRandomLayer.py**
* **OSELM.py**
In order to create an OSELM model you can just do *from Codes.Models.OSELM import OSELMClassifier1* then create an instance of OSELMClassifier1 and send set the parameters you want with constructor.
Now for the remain files:
* **MahineLearningBuilderModule.py**
It is a files contains functions to create machine learning model creation for now it hold logisitic regression and decision trees you can add what ever machine learning models to it and play around with it
* **SimpleMLP.py**
This file holds a class called SimpleMLP that creates and build a simple MLP model which has a simple logic and straight forward way to create an instance of it you just need to know the input shape of your data and the number of classes you have pass them to it and it's done.
* **ProjectionModel.py**
This file contains a class called ProjectionModel which create a model that is used mainly with FedPCL algorithm but it can be used otherelse places, this class inherits from *tf.keras.Model* and override the function *call* which basically perform the forward pass of the model and modify the prediction of the model to return the features of the model and the classes probabilities in case of using a FedPCL model (client model or global model within the algorithm) it has a boolean varibale that decides what return for the other case when you want to create just centralized model you can set the boolen variable which is called *fed_approach* in the constructor to False and for sure you need to figure out the shapes of your data or in case you want to use a backbone to set it above the model you can just it to the model and for sure the number of your classes

6. **Visualization**:
The contents of this directory focus on extracting and plotting metrics or bars that describe each cluster's/group's data, enhancing the visual representation of the project's outcomes.

7. **Federated_Learning**:
This folder contain 3 main files:
- **FedLearning.py**
- **FedLearningAvg.py**
- **FedPCL.py**
both FedLearningAvg.py and FedPCL.py inherits from FedLearning.py and implement the methods that were shared between them but since FedLearningAvg.py contains the class of the FedLearningAvg that contains implementing all the components for the first phase of the Meta-Verse project, the FedPCL.py contains a class called FedPCL which implements the algorithm of FedPCL

9. **Utils**:
In this folder you will find only one file which is **losses.py** this file contains the implementation of losses were used within the FedPCL system.

* **Config.py**: Configuration file for the system.
* **main.py**: Script used in the previous phase of the project (may be outdated).
* **Notebooks**: Jupyter notebooks for running specific functionalities.
- **Copy of Federated Learning.ipynb**: Old notebook for the previous phase (potentially outdated).
- **FedPCL Centralized.ipynb**: Notebook for running the centralized model.
- **FedPCL Federated.ipynb**: Notebook for running the FedPCL algorithm.
- **FedPCL Testing Notebook.ipynb**: Notebook for testing purposes (might be messy).

## Code Style and Clarity

Each folder contains well-organized classes with functions.
Functions are accompanied by comments explaining their purpose.

## Contact

Feel free to reach out for any inquiries or questions related to the code or project ideas.
