import numpy as np
import tensorflow as tf
import argparse
from Codes.Utils.losses import local_contrastive_loss, global_contrastive_loss


class Config:
  """
  Holds configuration variables for FedPCL implementation.
  """

  def __init__(self):
    # Data paths
    self.dataset_path = "/content/drive/MyDrive/meta-verse/data/image_dataset"
    self.test_dataset_path = "No testing data path for now"

    # Base folders for results and models
    self.base_folder_centralized_results = "/content/drive/MyDrive/meta-verse/Results and Plots/FedPCL/Centralized"
    self.base_folder_centralized_models = "/content/drive/MyDrive/meta-verse/Results and Plots/FedPCL/Centralized/Models"
    self.base_folder_centralized_plots = "/content/drive/MyDrive/meta-verse/Results and Plots/FedPCL/Centralized/Plots"

    self.base_folder_fedpcl_results = "/content/drive/MyDrive/meta-verse/Results and Plots/FedPCL/Fed/Hetero"
    self.base_folder_fedpcl_models_client = "/content/drive/MyDrive/meta-verse/Results and Plots/FedPCL/Fed/Hetero/Models/Clients"
    self.base_folder_fedpcl_models_global = "/content/drive/MyDrive/meta-verse/Results and Plots/FedPCL/Fed/Hetero/Models/Global"
    self.base_folder_fedpcl_plots_global = "/content/drive/MyDrive/meta-verse/Results and Plots/FedPCL/Fed/Hetero/Plots/Global"
    self.base_folder_fedpcl_plots_clients = "/content/drive/MyDrive/meta-verse/Results and Plots/FedPCL/Fed/Hetero/Plots/Clients"

    # Data and model parameters
    self.data_name = "DAISEE"
    self.metrics = ["accuracy"]
    self.loss = "categorical_crossentropy"
    self.input_shape = (224, 224, 3)
    # self.optimizer = "adam"
    self.number_of_classes = 4
    self.classes = np.arange(4)
    self.binarizer = 'None'

    # Training parameters
    self.clipping_norm = 10
    self.epsilon = 10
    self.comms_round = 25
    self.USERS_PER_ROUND = 10
    self.learning_rate = 1e-3
    self.momentum = 0.9
    self.number_of_users = 10
    self.batch_size = 32

    # Non-IID and privacy parameters
    self.non_iid_strength_factor = 3
    self.sigma = 2
    self.magic_value = 200
    self.gen_type = "hetero"
    self.the_allowed_hete = 2

    # Plotting and other flags
    self.plot = True
    self.differential_privacy = False
    self.clustering = False
    self.clients_curves = False
    self.clusters_histograms = False
    self.other_metrics = True

    # Define loss function and optimizer dictionaries (optional)
    self.loss_functions = {
        'categorical_crossentropy': tf.keras.losses.CategoricalCrossentropy(),
        'local_contrastive': local_contrastive_loss,  # Replace with your implementation
        'global_contrastrive': global_contrastive_loss  # Replace with your implementation
    }
    self.optimizers = {
        'adam': tf.keras.optimizers.legacy.Adam(learning_rate=1e-5, clipvalue=1.0),
    }

# def create_parser():

#     loss_functions = {
#         'categorical_crossentropy': tf.keras.losses.CategoricalCrossentropy(),
#         'local_contrastrive': local_contrastive_loss,
#         'global_contrastrive': global_contrastive_loss,
#     }
#     optimizers = {
#         'adam': tf.keras.optimizers.Adam(),
#     }
#     parser = argparse.ArgumentParser(description='FedPcl Implementation')

#     # data_paths = {
#     #     'dataset_path': "C:\\Users\lenovo\Downloads\Compressed\mnist_train.csv",
#     #     'test_dataset_path':"C:\\Users\lenovo\Downloads\Compressed\mnist_test.csv"
#     # }

#     parser.add_argument('--dataset_path', type=str, default="/content/drive/MyDrive/meta-verse/data/image_dataset")
#     parser.add_argument('--test_dataset_path', type=str, default="No testing data path for now")

#     parser.add_argument('--base_folder_centralized_results',
#                         type=str,
#                         default="/content/drive/MyDrive/meta-verse/Results and Plots/FedPCL/Centralized")
#     parser.add_argument('--base_folder_centralized_models',
#                         type=str,
#                         default="/content/drive/MyDrive/meta-verse/Results and Plots/FedPCL/Centralized/Models")
#     parser.add_argument('--base_folder_centralized_plots',
#                         type=str,
#                         default="/content/drive/MyDrive/meta-verse/Results and Plots/FedPCL/Centralized/Plots")

#     parser.add_argument('--base_folder_fedpcl_results',
#                         type=str,
#                         default="/content/drive/MyDrive/meta-verse/Results and Plots/FedPCL/Fed/Hetero")

#     parser.add_argument('--base_folder_fedpcl_models_client',
#                         type=str,
#                         default="/content/drive/MyDrive/meta-verse/Results and Plots/FedPCL/Fed/Hetero/Models/Clients")

#     parser.add_argument('--base_folder_fedpcl_models_global',
#                         type=str,
#                         default="/content/drive/MyDrive/meta-verse/Results and Plots/FedPCL/Fed/Hetero/Models/Global")

#     parser.add_argument('--base_folder_fedpcl_plots_global',
#                         type=str,
#                         default="/content/drive/MyDrive/meta-verse/Results and Plots/FedPCL/Fed/Hetero/Plots/Global")

#     parser.add_argument('--base_folder_fedpcl_plots_clients',
#                         type=str,
#                         default="/content/drive/MyDrive/meta-verse/Results and Plots/FedPCL/Fed/Hetero/Plots/Clients")

#     parser.add_argument('--data_name', type=str, default="DAISEE")
#     parser.add_argument('--metrics', nargs='+', default=['accuracy'], help='List of metrics')

#     parser.add_argument('--loss', default='categorical_crossentropy', choices=loss_functions.keys(),
#                         help='Loss function name')

#     parser.add_argument('--input_shape', nargs='+', type=int, default=[224, 224, 3],
#                         help='Input shape of the model (height, width, channels)')

#     parser.add_argument('--optimizer', default='adam', choices=optimizers.keys(),
#                         help='optimizer function name')

#     parser.add_argument('--number_of_classes', type=int, default=4,
#                         help='Number of classes')

#     parser.add_argument('--clipping_norm', type=int, default=10,
#                         help='Clipping norm')

#     parser.add_argument('--epsilon', type=int, default=10,
#                         help='Epsilon value')

#     parser.add_argument('--comms_round', type=int, default=25,
#                         help='Communication rounds')

#     parser.add_argument('--USERS_PER_ROUND', type=int, default=10,
#                         help='Number of users per round')

#     parser.add_argument('--learning_rate', type=float, default=0.001,
#                         help='Learning rate')

#     parser.add_argument('--momentum', type=float, default=0.9,
#                         help='Momentum')

#     parser.add_argument('--number_of_users', type=int, default=10,
#                         help='Number of users')

#     parser.add_argument('--batch_size', type=int, default=32,
#                         help='Batch size')

#     parser.add_argument('--non_iid_strength_factor', type=int, default=3,
#                         help='Non-IID strength factor')

#     parser.add_argument('--sigma', type=int, default=2,
#                         help='Sigma value')

#     parser.add_argument('--magic_value', type=int, default=200,
#                         help='Magic value')

#     parser.add_argument('--gen_type', type=str, default="hetero",
#                         help='Generation type')

#     parser.add_argument('--the_allowed_hete', type=int, default=2,
#                         help='Allowed heterogeneity')

#     parser.add_argument('--plot', type=bool, default=False,
#                         help='Plot flag')

#     parser.add_argument('--differential_privacy', type=bool, default=False,
#                         help='Differential privacy flag')

#     parser.add_argument('--clustering', type=bool, default=False,
#                         help='Clustering flag')

#     parser.add_argument('--clients_curves', type=bool, default=False,
#                         help='Clients curves flag')

#     parser.add_argument('--clusters_histograms', type=bool, default=False,
#                         help='Clusters histograms flag')

#     parser.add_argument('--other_metrics', type=bool, default=True,
#                         help='Other metrics flag')
#     return parser

# base_directories = {

#     'base_folder_centralized_results':'C:/Users/lenovo/Desktop/meta_verse/centralized/results',
#     'base_folder_centralized_models':'C:/Users/lenovo/Desktop/meta_verse/centralized/models',
    
#     'base_folder_extreme_results':'C:/Users/lenovo/Desktop/meta_verse/extreme/results/',
#     'base_folder_extreme_models':'C:/Users/lenovo/Desktop/meta_verse/extreme/models/',
#     'base_folder_extreme_groups':'C:/Users/lenovo/Desktop/meta_verse/extreme/groups/global_models/',
#     'base_folder_extreme_clients_data':'C:/Users/lenovo/Desktop/meta_verse/extreme/groups/clients_data/',
#     'base_folder_extreme_results_global_models':'C:/Users/lenovo/Desktop/meta_verse/extreme/results/global/',
#     'base_folder_extreme_results_clients_models':'C:/Users/lenovo/Desktop/meta_verse/extreme/results/clients/',

#     'base_folder_hetero_results':'C:/Users/lenovo/Desktop/meta_verse/hetero/results/',
#     'base_folder_hetero_models':'C:/Users/lenovo/Desktop/meta_verse/hetero/models/',
#     'base_folder_hetero_groups':'C:/Users/lenovo/Desktop/meta_verse/hetero/groups/global_models/',
#     'base_folder_hetero_clients_data':'C:/Users/lenovo/Desktop/meta_verse/hetero/groups/clients_data/',
#     'base_folder_hetero_results_global_models':'C:/Users/lenovo/Desktop/meta_verse/hetero/results/global/',
#     'base_folder_hetero_results_clients_models':'C:/Users/lenovo/Desktop/meta_verse/hetero/results/clients/',

#     'base_folder_homo_results':'C:/Users/lenovo/Desktop/meta_verse/homo/results/',
#     'base_folder_homo_models':'C:/Users/lenovo/Desktop/meta_verse/homo/models/',
#     'base_folder_homo_groups':'C:/Users/lenovo/Desktop/meta_verse/homo/groups/global_models/',
#     'base_folder_homo_clients_data':'C:/Users/lenovo/Desktop/meta_verse/homo/groups/clients_data/',

#     'base_folder_homo_results_global_models':'C:/Users/lenovo/Desktop/meta_verse/homo/results/global/',
#     'base_folder_homo_results_clients_models':'C:/Users/lenovo/Desktop/meta_verse/homo/results/clients/',
# }

# basic_variables = {
#     'data_name':'MNIST',
#     'metrics':['accuracy'],
#     'loss':tf.keras.losses.CategoricalCrossentropy(),
#     'input_shape':784,
#     'optimizer':tf.keras.optimizers.Adam(),
#     'client_batched':{},
#     'clients':{},
#     'number_of_classes':10,
#     'clipping_norm':10,
#     'epsilon':10,
#     'comms_round':25,
#     'USERS_PER_ROUND':10,
#     'learning_rate':0.001,
#     'momentum':0.9,
#     'number_of_users':10,
#     'batch_size':32,
#     'non_iid_strength_factor':3,
#     'sigma':2,
#     'magic_value':500,
#     'gen_type':'extreme',
#     'the_allowed_hete':2,
#     'plot':False,
#     'differential_privacy':False,
#     'clustering':True,
# }

# basic_variables_for_plotting ={
#     'differential_privacy':False,
#     'clustering':True,
#     'models_curves':True,
#     'clients_curves':False,
#     'clusters_histograms':True,
#     'other_metrics':True

# }

# changable_variables = {
#     'classes' : None,
#     'clients_data' : None,
#     'base_folder_results_global_models' : ' ',
#     'base_folder_results_clients_models' : ' ',
#     'base_folder_groups' : ' ',
#     'base_folder_results' : ' ',
#     'base_folder_models' : ' ',
#     'base_folder_clients_data' : ' ',
#     'groups_over_rounds' : None,
# }