import tensorflow as tf
import numpy as np
from tensorflow.keras import backend as K


def local_contrastive_loss(local_protos, temp):
    """Custom Keras loss function for local contrastive learning.

    Args:
        local_protos: A dictionary containing local prototypes for each class.
        temp: Temperature parameter for the dot product normalization.

    Returns:
        A Keras loss function that takes features and labels as inputs and returns the loss value.
    """

    def loss_fn(y_true, y_pred):
        """Calculates the local contrastive loss for a batch of features and labels.

        Args:
            y_true: Unused (placeholder for compatibility with Keras API).
            y_pred: The features for which to compute the loss.

        Returns:
            The local contrastive loss value for the batch.
        """
        features = y_pred
        features = tf.keras.layers.Lambda(lambda x: tf.nn.l2_normalize(x, axis=1))(features)
        batch_size = features.shape[0]
        contrast_count = features.shape[1]
        local_losses = 0
        for j in range(len(local_protos)):
          anchor_count = 1
          anchor_features = features.numpy()
          labels = tf.reshape(tf.argmax(y_true, axis=1), (-1, 1))
          mask = tf.cast(tf.equal(labels, tf.transpose(labels)), dtype=tf.float32)
          contrast_features = tf.concat(features, axis=1) 

          for i in range(0, batch_size * anchor_count):
            label_index = tf.cast(labels[i % batch_size], tf.int32)
            anchor_features[i,:] = local_protos[j][label_index.numpy().item()] 

          anchor_dot_contrast = tf.matmul(anchor_features, contrast_features, transpose_b=True)
          anchor_dot_contrast /= temp
          logits_max = tf.reduce_max(anchor_dot_contrast, axis=1, keepdims=True)
          logits = anchor_dot_contrast - logits_max

          mask = tf.tile(mask, [1, anchor_count])
          ones_like_mask = tf.ones_like(mask)
          indices = tf.range(batch_size)
          updates = tf.zeros_like(indices, dtype=tf.float32)
          logits_mask = tf.tensor_scatter_nd_update(ones_like_mask, tf.stack([indices, indices], axis=1), updates)
          mask = mask * logits_mask

          exp_logits = tf.exp(logits) * logits_mask + K.epsilon()
          exp_logits = tf.reduce_sum(exp_logits, axis=0, keepdims=True)
          log_sum_exp_logits = tf.math.log(exp_logits) + K.epsilon()
          log_prob = logits - log_sum_exp_logits
          mean_log_prob_pos = tf.reduce_sum(mask * log_prob, axis=0) / (tf.reduce_sum(mask, axis=0) + K.epsilon())
          loss = (temp) * mean_log_prob_pos
          loss = tf.reduce_mean(loss)
          local_losses += loss

        new_loss = -1/len(local_protos) * local_losses

        return new_loss
    return loss_fn


def global_contrastive_loss(server_protos, temp):
  """Custom Keras loss function for global contrastive learning.

    Args:
        server_protos: A dictionary containing local prototypes for each class.
        temp: Temperature parameter for the dot product normalization.

    Returns:
        A Keras loss function that takes features and labels as inputs and returns the loss value.
  """
  def loss_fn(y_true, y_pred):
    """Calculates the global contrastive loss for a batch of features and labels.

        Args:
            y_true: Unused (placeholder for compatibility with Keras API).
            y_pred: The features for which to compute the loss.

        Returns:
            The global contrastive loss value for the batch.
    """
    features = y_pred
    features = tf.keras.layers.Lambda(lambda x: tf.nn.l2_normalize(x, axis=1))(features)
    batch_size = features.shape[0]
    contrast_count = features.shape[1]
    anchor_count = 1
    anchor_features = features.numpy()
    labels = tf.reshape(tf.argmax(y_true, axis=1), (-1, 1))
    mask = tf.cast(tf.equal(labels, tf.transpose(labels)), dtype=tf.float32)
    contrast_features = tf.concat(features, axis=1) 

    for i in range(0, batch_size * anchor_count):
      label_index = tf.cast(labels[i % batch_size], tf.int32)
      anchor_features[i,:] = server_protos[label_index.numpy().item()] 

    anchor_dot_contrast = tf.matmul(anchor_features, contrast_features, transpose_b=True)
    anchor_dot_contrast /= temp
    logits_max = tf.reduce_max(anchor_dot_contrast, axis=1, keepdims=True)
    logits = anchor_dot_contrast - logits_max

    mask = tf.tile(mask, [1, anchor_count])
    ones_like_mask = tf.ones_like(mask)
    indices = tf.range(batch_size)
    updates = tf.zeros_like(indices, dtype=tf.float32)
    logits_mask = tf.tensor_scatter_nd_update(ones_like_mask, tf.stack([indices, indices], axis=1), updates)
    mask = mask * logits_mask

    exp_logits = tf.exp(logits) * logits_mask + K.epsilon()
    exp_logits = tf.reduce_sum(exp_logits, axis=0, keepdims=True)
    log_sum_exp_logits = tf.math.log(exp_logits) + K.epsilon()
    log_prob = logits - log_sum_exp_logits
    mean_log_prob_pos = tf.reduce_sum(mask * log_prob, axis=0) / (tf.reduce_sum(mask, axis=0) + K.epsilon())
    loss = - (temp) * mean_log_prob_pos
    loss = tf.reduce_mean(loss)

    return loss


  return loss_fn