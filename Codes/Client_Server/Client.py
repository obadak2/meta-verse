from Codes.Data.CreditScoringDataInduction import CreditScoringDataInduction
from Codes.Data.DaiseeDataInduction import DaiseeDataInduction
from Codes.Data.MnistDataInduction import MnistDataInduction
from Codes.Models.ProjectionModel import ProjectionNetwork
from sklearn.preprocessing import LabelBinarizer, binarize
from Codes.Models.MahineLearningBuilderModule import *
from typing import List, Tuple, Optional, Union, Any
from Codes.Models.OSELM import OSELMClassifier1
from Codes.Models.SimpleMLP import SimpleMLP

from Codes.Data.PreProcessing import *
import tensorflow as tf
import numpy as np

class Client:
    id = 0
    def __init__(self,
                 dataset_path: str,
                 batch_size: int,
                 data_name: str,
                 input_shape: Union[Tuple[int, int], int],
                 optimizer :str,
                 loss: str,
                 metrics: str,
                 number_of_users: int,
                 number_of_classes: int,
                 non_iid_strength_factor: int,
                 sigma: float,
                 classes: List[int],
                 feature_selection_method: str,
                 apply_feature_selection: str,
                 binarizer: Any,
                 ):
        self.id += 1
        self.binarizer = binarizer
        self.apply_feature_selection = apply_feature_selection
        self.feature_selection_method = feature_selection_method
        self.number_of_users = number_of_users
        self.dataset_path = dataset_path
        self.batch_size = batch_size
        self.data_name = data_name
        self.non_iid_strength_factor = non_iid_strength_factor

        self.sigma = sigma
        self.classes = classes
        if self.data_name == 'DAISEE':
          self.data_induction = DaiseeDataInduction(self.dataset_path,
                                                    self.batch_size,
                                                    self.data_name,
                                                    self.number_of_users,
                                                    self.non_iid_strength_factor,
                                                    self.sigma,
                                                    self.classes,
                                                    self.feature_selection_method,
                                                    self.apply_feature_selection
                                                    )
        elif self.data_name == 'MNIST':
          self.data_induction = MnistDataInduction(self.dataset_path,
                                                  self.batch_size,
                                                  self.data_name,
                                                  self.number_of_users,
                                                  self.non_iid_strength_factor,
                                                  self.sigma,
                                                  self.classes,
                                                  self.feature_selection_method,
                                                  self.apply_feature_selection
                                                  )
        elif self.data_name == 'creditscoring':
          self.data_induction = CreditScoringDataInduction(self.dataset_path,
                                                          self.batch_size,
                                                          self.data_name,
                                                          self.number_of_users,
                                                          self.non_iid_strength_factor,
                                                          self.sigma,
                                                          self.classes,
                                                          self.feature_selection_method,
                                                          self.apply_feature_selection
                                                          )
        self.input_shape = input_shape
        self.optimizer = optimizer
        self.loss = loss
        self.metrics = metrics
        self.number_of_classes = number_of_classes

    def get_user_id(self) -> int:
        """
         Get the user id. This is used to check if the user is logged in to the system.
         
         :returns: The user id or None if not logged in to the system ( in this case we don't have a user
        """
        return self.id

    def build_local_model_mnist(self) -> tf.keras.Model:
        """
         Build and compile MNIST SMLP model. This is a wrapper for SimpleMLP.
         
         :returns: A compiled and built MNIST SMLP model with optimization applied to it. It is used to train the model
        """
        smlp_local = SimpleMLP()
        local_model = smlp_local.build(self.input_shape, self.number_of_classes)
        local_model.compile(loss=self.loss, optimizer='Adam', metrics=self.metrics)
        return local_model

    def build_local_model_daisee(self, feature_extraction_model):
        """
         Build the daisee for the local model. It is called by DAISolver. build_
        """
        return ProjectionNetwork(input_shape=self.input_shape,
                                     number_of_classes=self.number_of_classes,
                                     extracted_features_shape = feature_extraction_model.output_shape)


    def build_model(self, feature_extraction_model:tf.keras.Model=None) -> Union[tf.keras.Model ,Any]:
        """
         Build model according to data_name. This is a wrapper for build_local_model_mnist or build_local_model_daisee
         
         :returns: a dictionary with keys : model_name : name of the
        """
        if self.data_name == 'MNIST':
            return self.build_local_model_mnist()
        elif self.data_name == 'DAISEE':
            return self.build_local_model_daisee(feature_extraction_model)
        else:
            model_name = self.data_name.split('_')[0]
            if model_name == 'faoselm':
              if self.binarizer=='custom':
                return OSELMClassifier1(15, activation_func='sigmoid', random_state=42, binarizer=CustomLabelBinirizer())

              elif self.binarizer=='none':
                return OSELMClassifier1(15, activation_func='sigmoid', random_state=42)
              
              else:
                return OSELMClassifier1(15, activation_func='sigmoid', random_state=42, binarizer=LabelBinarizer())
            
            elif model_name == 'tradoselm':
              if self.binarizer=='custom':
                return OSELMClassifier1(15, activation_func='relu', random_state=42, binarizer=CustomLabelBinirizer())

              elif self.binarizer=='none':
                return OSELMClassifier1(15, activation_func='relu', random_state=42)
              
              else:
                return OSELMClassifier1(15, activation_func='relu', random_state=42, binarizer=LabelBinarizer())
            
            elif model_name == 'dt':
                return Decision_Tree()
                
            else:
                return LogisticRegression_classifier()

    def read_full_daisee_data(self) -> Union[Tuple[tf.data.Dataset, tf.data.Dataset, tf.data.Dataset, List[int], List[int]], tf.data.Dataset]:
        """
         Read data from daisee. This is a wrapper around : py : meth : ` ~data_induction. read_data ` to allow data to be read from a file
         
         :returns: a dataset with the
        """
        dataset = self.data_induction.read_data()
        return dataset

    def read_full_mnist_data(self) -> List[tf.data.Dataset]:
        """
         Reads data from MNIST and returns it. This is a wrapper for data_induction. read_data
         
         :returns: list of clients that were
        """
        clients_data_updated = self.data_induction.read_data()
        return clients_data_updated

    def read_batched_mnist_data(self, clients_data_updated: List[tf.data.Dataset]) -> List[tf.data.Dataset]:
        """
         Reads MNIST data from HDF5 and returns a dataset. This is a wrapper around the DataInduction. read_batched_data method which uses the number of classes to read 
         
         :param clients_data_updated: list of clients that have been updated
         :returns: dataset of training data ( numpy array ) and validation data ( numpy array ) for each client ( int
        """
        train_dataset = self.data_induction.read_batched_data(self.number_of_classes,
                                                              clients_data_updated=clients_data_updated)
        return train_dataset

    def read_batched_equal_split_mnist_data(self, clients_data_upda :List[tf.data.Dataset]=None) -> List[tf.data.Dataset]:
        """
         Read batched equal MNIST data. This is a wrapper around : py : meth : ` ~data_induction. read_batched_equal_split_mnist_data ` to avoid having to re - read the data in a single call.
         
         :param clients_data_upda: Updatable data that was used to train the model.
         :returns: A list of : py : class : ` ~astropy. table. Table ` objects one for each client
        """
        return self.data_induction.read_batched_equal_split_mnist_data(clients_data_upda=clients_data_upda)

    def read_batched_daisee_data(self, user_id: int) -> List[tf.data.Dataset]:
        """
         Read batched daisee data. This is a convenience method for calling : py : meth : ` DataInduction. read_batched_data ` on
         
         :param user_id: The id of the user.
         :returns: A list of data in the format returned by : py : meth : ` DataInduction. read
        """
        dataset = self.data_induction.read_batched_data(user_id)
        return dataset
    
    def read_batched_credit_scoring_data(self,
                                        user_id: int,
                                        data_length: int,
                                        data: List[tf.data.Dataset],
                                        label_list: List[int]) -> List[tf.data.Dataset]:
        """
         Read batched credit scoring data. This is a convenience method for calling : py : meth : ` DataInduction. read_batched_data ` on
         
         :param user_id: The id of the user.
         :returns: A list of data in the format returned by : py : meth : ` DataInduction. read
        """
        dataset = self.data_induction.read_batched_credit_scoring_data(user_id, data_length)
        user_data = np.array(self.data_induction.read_batched_credit_scoring_data(user_id, data_length)).astype(int)
        # user_data = self.data_induction.process_clients_data(label_list, user_data)
        user_data = self.data_induction.retrieve_data_from_indices(data, label_list, user_data)
        return user_data

    def read_nor_data(self, magic_value: int, mu: float, images: List[Union[List[float], float]], label_list: List[int], start: int, end: int, gen_type: str, act: int) -> List[tf.data.Dataset]:
        """
        Generate data based on the type that is desired as (Heterogeneous, Extreme Heterogeneous, Homogeneous) 
        
        :param magic_value: Value that is used to define the number of samples to be added for each class or for the selected class in case of generating heterogeneous data based on normal distribution
        :param mu: Holds the centralized class for generating heterogeneous data based on normal distribution 
        :param images: List of images to be processed
        :param label_list: List of labels for each image ( 0 - indexed )
        :param start:
        :param end:
        :param gen_type: Type of generation (Heterogeneous, Extreme Heterogeneous, Homogeneous)
        :param act: 
        :returns: User data of shape ( nb_samples n_features ) where n_samples is the number of samples
        """
        user_data = np.array(self.data_induction.produce_data_normal_dis(magic_value,
                                                                         mu,
                                                                         start=start,
                                                                         end=end,
                                                                         gen_type=gen_type,
                                                                         act=act,
                                                                         labels_sums=np.sum(label_list, axis=0)
                                                                         )).astype(int)
        user_data = self.data_induction.process_clients_data(label_list, user_data)
        user_data = self.data_induction.retrieve_data_from_indices(images, label_list, user_data)

        return user_data

    def read_batched_data(self,
                          magic_value: int=None,
                          mu: int=None,
                          images: List[Union[List[float], float]]=None,
                          label_list: List[int]=None,
                          user_id: int=None,
                          clients_data_updated: List[tf.data.Dataset]=None,
                          start: int=0,
                          end: int=2,
                          act: int=2,
                          split_type: str='homo',
                          data_length: int=None) -> List[tf.data.Dataset]:
        """
        Read data from NOR. This is a wrapper around read_nor_data to handle MNIST split types
        
        :param magic_value: Value that is used to define the number of samples to be added for each class or for the selected class in case of generating heterogeneous data based on normal distribution
        :param mu: Holds the centralized class for generating heterogeneous data based on normal distribution 
        :param images: List of images to use for missing data ( default None )
        :param label_list: List of labels to use for missing data ( default None )
        :param user_id: User ID to use for missing data ( default None )
        :param clients_data_updated: Data updated by client ( default None
        :param start
        :param end
        :param act
        :param split_type
        """
        if self.data_name == 'MNIST':
            if split_type == 'traditional':
                return self.read_batched_mnist_data(clients_data_updated)
            elif split_type == 'extreme':
                return self.read_nor_data(magic_value, mu, images, label_list, start, end, split_type, act)
            elif split_type == 'hetero':
                return self.read_nor_data(magic_value, mu, images, label_list, start, end, split_type, act)
            elif split_type == 'homo':
                return self.read_nor_data(magic_value, mu, images, label_list, start, end, split_type, act)

        elif self.data_name == 'DAISEE':
            return self.read_nor_data(magic_value, mu, images, label_list, start, end, split_type, act)
        else:
            return self.read_batched_credit_scoring_data(user_id, data_length, images, label_list)

    def read_full_data(self) -> tf.data.Dataset:
        """
         Read data from the data induction. This is useful for debugging and to avoid having to re - read the data every time it is read.
         
         :returns: A list of numpy arrays one for each data point in the data induction. The list is sorted by the time
        """
        return self.data_induction.read_data()

    def get_local_model_wights(self, model: tf.keras.Model) -> np.ndarray[np.ndarray]:
        """
         Get the local model weights. This is used to determine whether or not we are going to do an iterative search or not
         
         :param model: The model to be analysed
         :returns: A list of weight values that are in the local model for this iteration or None if there are no
        """
        return model.get_weights()

    def update_local_model_weights(self, model: tf.keras.Model, weights: np.ndarray[np.ndarray]):
        """
         Update the weights of a LocalModel. This is used to update the weights of a LocalModel before it is passed to the model's run method.
         
         :param model: The model to update. Must be of type : py : class : ` pyspark. sql. types. Model `.
         :param weights: The new weights to set. Must be of type : py : class : ` pyspark. sql. types. Weight `.
         :returns: The updated model. Note that the weights are updated in - place and not returned by this method. It is up to the caller to make sure they are in - place
        """
        model = model.set_weights(weights)
        return model

    def get_data_size(self) -> int:
        """
         Get the size of the data. This is used to determine how many rows and columns are stored in the data_induction.
         
         :returns: The size of the data in bytes or None if there is no data in the data_induction
        """
        return self.data_induction.get_data_size()

    def prepare_test_data(self, test_dataset_path: str='str', test_data: Tuple[List[str], List[List[int]]]=None) -> tf.data.Dataset:
        """
         Prepares test data for use. This is a wrapper around the DataInduction. prepare_test_data method to be able to pass a path to the test dataset and it will return a dictionary of data that can be used to test the data.
         
         :param test_dataset_path: The path to the test dataset.
         :returns: A dictionary of data that can be used to test the data. The keys of the dictionary are the names of the test dataset
        """
        if self.data_name == 'MNIST':
          return self.data_induction.prepare_test_data(test_dataset_path)
        elif self.data_name == 'DAISEE':
          return self.data_induction.prepare_test_val_data(test_data)

    def take_batch_of_data(self, data: List[tf.data.Dataset]) -> Tuple[List[tf.data.Dataset], List[tf.data.Dataset]]:
        return self.data_induction.take_batch_of_data(data)

