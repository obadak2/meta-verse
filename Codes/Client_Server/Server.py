from Codes.Models.ProjectionModel import ProjectionNetwork
from Codes.Data.PreProcessing import CustomLabelBinirizer
from Codes.Models.MahineLearningBuilderModule import *
from Codes.Models.OSELM import OSELMClassifier1
from Codes.Models.SimpleMLP import SimpleMLP
from tensorflow.keras.models import Model
import tensorflow as tf
import numpy as np
from typing import List, Tuple, Optional, Union, Any


class Server:
    def __init__(self, input_shape: Union[Tuple[int, int], int], data_name: str, number_of_classes: int, binarizer: Any=None):
        self.data_name = data_name
        self.input_shape = input_shape
        self.number_of_classes = number_of_classes
        self.binarizer = binarizer

    def build_server_model_for_mnist(self) -> tf.keras.Model:
        """
         Builds MNIST model for use in MLP server. This is a helper function to be used in server_model. py
         
         :returns: server model for use in
        """
        smlp_global = SimpleMLP()
        global_model_favg = smlp_global.build(self.input_shape, self.number_of_classes)
        return global_model_favg

    def compile_fit_model(self, model: Union[tf.keras.Model ,Any], data: tf.data.Dataset, metrics: str, loss_function: str, epochs: int=1, non_selected_features: List[int]=None, selected_features: List[int]=None, percentage_to_train_on: int=0.1) -> tf.keras.Model:
        """
         Compile and fit a model. This is the function that is called by fit_model. The data is passed as a list of 2 - tuples. The first tuple is the training data and the second tuple is the label, could be of type Tesnoreflow dataset.
         
         :param model: The model to be trained.
         :param data: The data to be used for training the model.
         :param metrics: The metrics to be used for training the model.
         :param loss_function: The loss function to be used for training the model.
         :param percentage_to_train_on: The percentage of data to be used for training.
         :returns: A tuple of ( loss_function data ) where loss_function is a function that takes a data and returns a loss
        """
        if self.data_name == 'DAISEE':
            pass
        elif self.data_name == 'MNIST':
            model.compile(loss=loss_function,
                      optimizer='Adam',
                      metrics=metrics)
            his = model.fit(data, epochs=epochs, verbose=1)
            return his, model
        else:
            test_data_data = []
            test_data_label = []
            for element in list(data):
              for data1 in element[0]:
                  test_data_data.append(data1.numpy())
              for label in element[1]:
                  test_data_label.append(label.numpy())
            if self.data_name.split('_')[0] == 'faoselm':
                model.fit(test_data_data, test_data_label, non_selected_features)
            
            return model

    def get_server_model_weights(self, model: tf.keras.Model) -> np.ndarray[np.ndarray]:
        """
         Returns the weights associated with the server model. This is a convenience method for calling the get_weights method of the given model and returning it.
         
         :param model: The model to get the weights for. Must be a : py : class : ` ~pysimm. amalgamation. Server `
         :returns: A list of : py : class : ` ~pysimm. amalgamation. Server `
        """
        return model.get_weights()

    def update_server_model_weights(self, model: tf.keras.Model, weights: np.ndarray[np.ndarray]):
        """
         Update weights of a server model. This is used to update the weights of a server model that is passed as an argument to the server_model method.
         
         :param model: The model to update. Must be of type ServerModel.
         :param weights: The new weights to set. Must be of type float.
         :returns: The updated model. Note that it is a copy of the model passed as an argument and will be returned
        """
        model = model.set_weights(weights)
        return model

    def build_local_model_daisee(self, feature_extraction_model):
        """
         Build the model for using DAISEE dataset.
        """
        return ProjectionNetwork(input_shape=self.input_shape,
                                     number_of_classes=self.number_of_classes,
                                     extracted_features_shape = feature_extraction_model.output_shape)

    def build_model(self, feature_extraction_model) -> Union[tf.keras.Model ,Any]:
        """
         Build model for MNIST. This is a wrapper around build_server_model_for_mnist to be able to add a model to the server
         
         :returns: a dict with keys : data_name : name of the
        """
        if self.data_name == 'DAISEE':
            return self.build_local_model_daisee(feature_extraction_model)

        elif self.data_name == 'MNIST':
            return self.build_server_model_for_mnist()
        else:
            model_name = self.data_name.split('_')[0]
            if model_name == 'faoselm':
              if self.binarizer=='custom':
                return OSELMClassifier1(15, activation_func='sigmoid', random_state=42, binarizer=CustomLabelBinirizer())

              elif self.binarizer=='none':
                return OSELMClassifier1(15, activation_func='sigmoid', random_state=42)
              
              else:
                return OSELMClassifier1(15, activation_func='sigmoid', random_state=42, binarizer=LabelBinarizer())
            
            elif model_name == 'tradoselm':
              if self.binarizer=='custom':
                return OSELMClassifier1(15, activation_func='relu', random_state=42, binarizer=CustomLabelBinirizer())

              elif self.binarizer=='none':
                return OSELMClassifier1(15, activation_func='relu', random_state=42)
              
              else:
                return OSELMClassifier1(15, activation_func='relu', random_state=42, binarizer=LabelBinarizer())
            
            elif model_name == 'dt':
                return Decision_Tree()
            else:
                return LogisticRegression_classifier()
