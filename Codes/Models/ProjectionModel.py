from keras.layers import Dense, Dropout, Activation, BatchNormalization, Conv2D, MaxPooling2D, Flatten
from typing import List, Optional, Tuple, Any
import tensorflow as tf
import numpy as np

class ProjectionNetwork(tf.keras.Model):
    def __init__(self,
                 *,
                  input_shape: Tuple[int, int, int],
                  number_of_classes: int,
                  extracted_features_shape: np.ndarray[np.ndarray]=None,
                  filters: int = 64,
                  activation: str = 'relu',
                  output_activation: str = 'softmax',
                  dropout_rate: float = 0.25,
                  fed_approach: bool=True) -> None:
        """
        Initializes the model with a Sequential model and stores the input shape.

        Args:
            input_shape: A tuple representing the input shape of the model
                (height, width, channels).
        """
        super(ProjectionNetwork, self).__init__()
        # self.model = tf.keras.Sequential()
        self.the_input_shape = input_shape
        self.extracted_features_shape = extracted_features_shape

        if self.extracted_features_shape is None:
          self.dense_layer_input = Dense(filters, input_shape=self.the_input_shape)
        else:
          self.dense_layer_input = Dense(filters, input_shape=self.extracted_features_shape[1:])
        self.batch_normalization = BatchNormalization()
        self.act = Activation(activation)
        self.output_act = Activation(output_activation)
        self.dropout = Dropout(dropout_rate)
        self.flatten = Flatten()
        self.linear1 = Dense(512)
        self.linear2 = Dense(256)
        self.linear3 = Dense(128)
        self.linear4 = Dense(64)
        self.the_output = Dense(number_of_classes)
        self.fed_approach = fed_approach


    def call(self, x):
      x = self.dense_layer_input(x)
      x = self.batch_normalization(x)
      x = self.act(x)
      x = self.dropout(x)
      x = self.batch_normalization(x)
      x = self.act(x)
      x = self.dropout(x)
      x = self.linear4(x)
      x = self.act(x)
      _returned_features = self.batch_normalization(x)
      x = self.act(_returned_features)
      x = self.batch_normalization(x)
      x = self.the_output(x)
      _outputs = self.output_act(x)

      if self.fed_approach:
        return _returned_features, _outputs
      else:
        return _outputs

    # add this function to get correct output for model summary
    def summary(self):
        x = tf.keras.Input(shape=self.the_input_shape, name="input_layer")
        model = tf.keras.Model(inputs=[x], outputs=self.call(x))
        return model.summary()