from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.preprocessing import LabelBinarizer
from abc import ABCMeta, abstractmethod
from sklearn.base import BaseEstimator
from sklearn.utils import check_array
import numpy as np

class BaseRandomLayer(BaseEstimator, TransformerMixin):
    """Abstract Base class for random layers"""
    __metaclass__ = ABCMeta

    _internal_activation_funcs = dict()

    @classmethod
    def activation_func_names(cls):
        """Get list of internal activation function names"""
        return cls._internal_activation_funcs.keys()

    def __init__(self,
                 n_hidden=20,
                 random_state=0,
                 activation_func=None,
                 activation_args=None,):

        self.n_hidden = n_hidden
        self.random_state = random_state
        self.activation_func = activation_func
        self.activation_args = activation_args

        self.components_ = dict()
        self.input_activations_ = None

        # keyword args for internally defined funcs
        self._extra_args = dict()

    @abstractmethod
    def _generate_components(self, X):
        """Generate components of hidden layer given X"""

    @abstractmethod
    def _compute_input_activations(self, X, non_selected_features=None):
        """Compute input activations given X"""

    def _compute_hidden_activations(self, X, non_selected_features=None):
        """Compute hidden activations given X"""
        # compute input activations and pass them
        # through the hidden layer transfer functions
        # to compute the transform

        self._compute_input_activations(X, non_selected_features)

        acts = self.input_activations_

        if callable(self.activation_func):
            args_dict = self.activation_args if self.activation_args else {}
            X_new = self.activation_func(acts, **args_dict)
        else:
            func_name = self.activation_func
            func = self._internal_activation_funcs[func_name]
            X_new = func(acts, **self._extra_args)

        return X_new

    def fit(self, X, y=None):
        """Generate a random hidden layer.

        Parameters
        ----------
        X : {array-like, sparse matrix} of shape [n_samples, n_features]
            Training set: only the shape is used to generate random component
            values for hidden units

        y : not used: placeholder to allow for usage in a Pipeline.

        Returns
        -------
        self
        """
        # perform fit by generating random components based
        # on the input array
        X = check_array(X, accept_sparse=True)
        self._generate_components(X)

        return self

    def transform(self, X, non_selected_features=None, y=None):
        """Generate the random hidden layer's activations given X as input.

        Parameters
        ----------
        X : {array-like, sparse matrix}, shape [n_samples, n_features]
            Data to transform

        y : not used: placeholder to allow for usage in a Pipeline.

        Returns
        -------
        X_new : numpy array of shape [n_samples, n_components]
        """
        # perform transformation by calling compute_hidden_activations
        # (which will normally call compute_input_activations first)
        X = check_array(X, accept_sparse=True)

        if len(self.components_) == 0:
            raise ValueError('No components initialized')

        return self._compute_hidden_activations(X, non_selected_features)
