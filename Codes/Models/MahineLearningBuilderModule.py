from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, classification_report
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn import metrics


def Decision_Tree():
    dt = DecisionTreeClassifier(max_depth=5) #.fit(X_train, Y_train)
    # tree_predicted = dt.predict(X_test)

    return dt

    # print(classification_report(Y_test, tree_predicted, target_names=['class1', 'class2']))
    # return accuracy_score(Y_test, tree_predicted), precision_score(Y_test, tree_predicted, average='weighted') \
    # , recall_score(Y_test, tree_predicted, average='weighted'), f1_score(Y_test, tree_predicted, average='weighted'), tree_predicted


def LogisticRegression_classifier():
    logreg = LogisticRegression()

    return logreg
    # logreg.fit(X_train, Y_train)
    # pred = logreg.predict(X_test)
    # print(classification_report(Y_test, pred, target_names=['class1', 'class2']))
    # return accuracy_score(Y_test, pred), precision_score(Y_test, pred, average='weighted'),\
    #  recall_score(Y_test, pred, average='weighted'), f1_score(Y_test, pred, average='weighted'), pred