from Codes.Data.InductionForData import InductionForData
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.preprocessing import LabelBinarizer
from sklearn.preprocessing import OneHotEncoder
from sklearn.linear_model import Lasso
from Codes.Data.PreProcessing import *
from tqdm.notebook import tqdm as tq
from collections import Counter
from scipy.stats import norm
import tensorflow as tf
import pandas as pd
import numpy as np
import pickle
import random
import copy
import os
from typing import List, Tuple, Optional, Union


class DaiseeDataInduction(InductionForData):

    def __init__(self, dataset_path: str,
                 batch_size: int,
                 data_name: str,
                 number_of_users: int,
                 non_iid_strength_factor: int,
                 sigma: float,
                 classes: List[int],
                 feature_selection_method: str,
                 apply_feature_selection: str
                 ):
        super(DaiseeDataInduction).__init__()
        self.apply_feature_selection = apply_feature_selection
        self.feature_selection_method = feature_selection_method
        self.dataset_path = dataset_path
        self.batch_size = batch_size
        self.data_name = data_name
        self.number_of_users = number_of_users
        self.non_iid_strength_factor = non_iid_strength_factor
        self.sigma = sigma
        self.classes = classes
        self.selected_indices = []

    
    def read_full_daisee_data(self) -> tf.data.Dataset:
        """
         Read data from daisee and return it as tensorflow batched dataset
        """
        full_path_to_modified_saved_paths = self.dataset_path + '/data_labels_pairs_paths_modified.pkl'
        if not os.path.exists(full_path_to_modified_saved_paths):
          original_imgs_path = f"{self.dataset_path}/original_imgs"
          original_data_assuming, labels = self.load_imgs_paths_with_labels_as_pairs(original_imgs_path)
          new_original_data_assuming, _ = self.get_samples_by_label(original_data_assuming, 0, int(Counter(labels)[0]/2))
          a_file = open(full_path_to_modified_saved_paths, "wb")
          pickle.dump(new_original_data_assuming, a_file)
        else:
          with open(full_path_to_modified_saved_paths, 'rb') as pickled_file:
            new_original_data_assuming = pickle.load(pickled_file)

        imagesmm, labelsmm = zip(*new_original_data_assuming)

        encoder = OneHotEncoder(sparse_output=False)

        labelsmm = encoder.fit_transform(np.array(labelsmm).reshape(-1, 1))
        train_imagesmm, test_imagesmm, train_labelsmm, test_labelsmm = train_test_split(imagesmm, labelsmm, test_size=0.2, random_state=42, stratify=labelsmm)
        train_data = list(zip(train_imagesmm, train_labelsmm))
        test_data = list(zip(test_imagesmm, test_labelsmm))

        return train_data, test_data
    
    def prepare_test_val_data(self, data):
      imagesmm, labelsmm = zip(*data)
      test_imagesmm, val_imagesmm, test_labelsmm, val_labelsmm = train_test_split(imagesmm, labelsmm, test_size=0.2, random_state=42, stratify=labelsmm)
      
      test_data = list(zip(test_imagesmm, test_labelsmm))
      val_images = []
      for i in range(len(val_imagesmm)):
        loaded_img = np.load(val_imagesmm[i])
        val_images.append(loaded_img)

      val_data = list(zip(val_images, val_labelsmm))

      return val_data, test_data

    def read_batched_daisee_data_using_userId(self, user_id: int) -> tf.data.Dataset:
        """
         Read batched daisee data. This method is called to read the data for a user.
         
         :param user_id: ID of the user for whom data is
        """
        labels = []
        image_data = []
        labels_to_number = {'Engagement': 0,
                            'Bordem': 1,
                            'Confusion': 2,
                            'Frustation ': 3,
                            'not_found_label': 4}
        new_path = self.dataset_path
        images = os.listdir(new_path)
        for image in tq(images):
            if user_id in image:
                path_to_image = os.path.join(new_path, image)
                label = image.split('_')
                label = label[-1].split('.')[0]
                try:
                    image = np.load(path_to_image, allow_pickle=True)
                except Exception:
                    try:
                        image = np.load(path_to_image, allow_pickle=False)
                    except Exception as e:
                        print(f"Error loading {image}: {str(e)}")
                        continue

                image_data.append(image)
                labels.append(label)

                dataset = tf.data.Dataset.from_tensor_slices((list(image_data), list(labels)))
            else:
                continue

        return dataset.shuffle(len(label)).batch(self.batch_size)
      

    def get_daisee_image(self, test_data_tuple: Tuple[List[str], list[int]]) -> Tuple[np.ndarray, int]:
        """
         Get daisee image. 

         :param test_data_path: Path to the test data
        """
        labels_to_number = {'Engagement': 0,
                            'Bordem': 1,
                            'Confusion': 2,
                            'Frustation ': 3,
                            'not_found_label': 4}
        img = np.random.choice(test_data_tuple)
        path_to_image = os.path.join(img)

        label = path_to_image[1]
        path_to_image = path_to_image[0]
        try:
            image = np.load(path_to_image, allow_pickle=True)
        except Exception:
            try:
                image = np.load(path_to_image, allow_pickle=False)
            except Exception as e:
                print(f"Error loading {image}: {str(e)}")
        return image, label

    def get_daisee_dataset_size(self) -> int:
        new_path = self.dataset_path
        images = os.listdir(new_path)
        return len(images)

    def prepare_test_data(self, test_data_tuple: Tuple[List[str], list[int]]) -> tf.data.Dataset:
        x_data = []
        y_data = []
        for path, label in test_data_tuple:
          try:
            image = np.load(path, allow_pickle=True)
            x_data.append(image)
          except Exception:
              try:
                  image = np.load(path, allow_pickle=False)
                  x_data.append(image)
              except Exception as e:
                  print(f"Error loading {image}: {str(e)}")
          y_data.append(label)

        lb = LabelBinarizer()
        label_list = lb.fit_transform(y_data)

        test_dataset = tf.data.Dataset.from_tensor_slices((x_data, label_list)).batch(self.batch_size)

        return test_dataset

    def retrieve_data_from_indices(self, images, labels, indices):
        """
         Retrieve data from indices.
         
         :param images: A list of images to be used for retrieval.
         :param labels: A list of labels to be used for retrieval.
         :param indices: A list of indices to be retrieved from the images
        """
        flattened_data_images = []
        flattened_data_labels = []

        for i in range(len(indices)):
            if isinstance(images[0], str):
              selected_images = np.array(images)[indices[i]]
              for img in selected_images:
                loaded_img = np.load(img)
                flattened_data_images.append(np.array(loaded_img))
            else:
              flattened_data_images.append(np.array(images)[indices[i]])
            flattened_data_labels.append(np.array(labels)[indices[i]])

        # flattened_data_images = [item for sublist in flattened_data_images for item in sublist]
        flattened_data_labels = [item for sublist in flattened_data_labels for item in sublist]

        dataset = tf.data.Dataset.from_tensor_slices((flattened_data_images, flattened_data_labels))

        return dataset.shuffle(len(flattened_data_labels)).batch(self.batch_size)

  