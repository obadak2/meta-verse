from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.preprocessing import LabelBinarizer
from sklearn.preprocessing import OneHotEncoder
from sklearn.linear_model import Lasso
from Codes.Data.PreProcessing import *
from tqdm.notebook import tqdm as tq
from collections import Counter
from scipy.stats import norm
import tensorflow as tf
import pandas as pd
import numpy as np
import pickle
import random
import copy
import os
from typing import List, Tuple, Optional, Union


class InductionForData:

    def __init__(self, dataset_path: str,
                 batch_size: int,
                 data_name: str,
                 number_of_users: int,
                 non_iid_strength_factor: int,
                 sigma: float,
                 classes: List[int],
                 feature_selection_method: str,
                 apply_feature_selection: str
                 ):
        self.apply_feature_selection = apply_feature_selection
        self.feature_selection_method = feature_selection_method
        self.dataset_path = dataset_path
        self.batch_size = batch_size
        self.data_name = data_name
        self.number_of_users = number_of_users
        self.non_iid_strength_factor = non_iid_strength_factor
        self.sigma = sigma
        self.classes = classes
        self.selected_indices = []

    def process_clients_data(self, label_list: List[int], client):
        """
         Process data sent from clients. This is called after the client has been created and is ready to be sent to the client's client_data attribute.
         
         :param label_list: List of labels that have been assigned to the client.
         :param client: Client that is going to be processed by this plugin
        """

        clients_all_labels = []

        for idx, label in enumerate(client):
            client_label_zero = []
            i = 0

            ss = np.where(np.argmax(label_list, axis=1) == idx)[0]

            while i < client[idx]:
                if ss[i] not in self.selected_indices:
                    client_label_zero.append(ss[i])
                    self.selected_indices.append(ss[i])
                    i += 1
                else:
                    i += 1
                    client[idx] += 1
                    continue

            clients_all_labels.append(client_label_zero)

        return clients_all_labels

    def preprocess_clients_data_fs(self, X_train: Union[np.ndarray, pd.DataFrame], y_train: Union[np.ndarray, pd.DataFrame]) -> Tuple[List[Union[int, str]], List[Union[int, str]]]:
        """
         Process data sent from clients. This is called after the client has been created and is ready to be sent to the client's client_data attribute.
         In the follwoing fuction we only return the indices of selected and the non selected features based on the previous building of the feature selection codes

         
         :param label_list: List of labels that have been assigned to the client.
         :param client: Client that is going to be processed by this plugin
        """
        if self.feature_selection_method=='single_agent':
          non_selected_features_var = non_selected_features(X_train)
          selected_features_var = selected_features(X_train)

          return non_selected_features_var, selected_features_var

        elif self.feature_selection_method=='lasso':
          if len(X_train.shape)==1:
            X_train = X_train.reshape(-1, 1)
            y_train = y_train.reshape(-1, 1)

          non_selected_features_var = non_selected_features_SelectBest(X_train, y_train, Lasso(alpha=0.0001, random_state=42, max_iter=100))
          selected_features_var = selected_features_SelectBest(X_train, y_train, Lasso(alpha=0.0001, random_state=42, max_iter=100))

          return non_selected_features_var, selected_features_var

        elif self.feature_selection_method=='rf':
          if len(X_train.shape)==1:
            X_train = X_train.reshape(-1, 1)
            y_train = y_train.reshape(-1, 1)
          non_selected_features_var = non_selected_features_SelectBest(X_train, y_train, RandomForestClassifier(n_estimators = 20))
          selected_features_var = selected_features_SelectBest(X_train, y_train, RandomForestClassifier(n_estimators = 20))

          return non_selected_features_var, selected_features_var
        else:
          if len(X_train.shape)==1:
            X_train = X_train.reshape(-1, 1)
            y_train = y_train.reshape(-1, 1)
          N = X_train.shape[1]
          non_selected_features_var = non_select_best_features_RFE(X_train, y_train, int(N*0.3),int(N/10))
          selected_features_var = select_best_features_RFE(X_train, y_train, int(N*0.3),int(N/10))


          return non_selected_features_var, selected_features_var

    def retrieve_data_from_indices(self, images: List[Union[List[float], float]], labels: List[int], indices: List[int]) -> tf.data.Dataset:
        """
         Retrieve data from indices.
         
         :param images: A list of images to be used for retrieval.
         :param labels: A list of labels to be used for retrieval.
         :param indices: A list of indices to be retrieved from the images
        """
        pass

    def produce_data_normal_dis(self, magic_value: int, mu: float, start: int=None, end: int=None, gen_type: str='hetero', act: int=2,
                                labels_sums: List[int]=None):
        """
         Produce data of normal distribution . 
         
        :param magic_value: Value that is used to define the number of samples to be added for each class or for the selected class in case of generating heterogeneous data based on normal distribution
        :param mu: Holds the centralized class for generating heterogeneous data based on normal distribution 
        :param start: 
        :param end:
        :param gen_type: Type of random numbers to generate(Heterogeneous, Extreme Heterogeneous, Homogeneous)
        :param act:
        :param labels_sums: List of labels summed over time
        """

        clients_data = []
        classes = copy.deepcopy(self.classes)
        ranged_values = [0 for _ in range(len(classes))]

        if gen_type == 'hetero':
            pdf_values = norm.pdf(classes, loc=mu, scale=self.sigma)
            max_pdf = max(pdf_values)
            pdf_values = pdf_values / max_pdf
            for i in range(magic_value):
                for x, scaled_pdf in zip(classes, pdf_values):
                    ranged_values[int(x)] += scaled_pdf

        if gen_type == 'extreme':
            classes = classes[start:end]
            pdf_values = np.random.uniform(1, 1, len(classes))
            for x, scaled_pdf in zip(classes, pdf_values):
                for _ in range(labels_sums[int(x)] // act):
                    ranged_values[int(x)] += scaled_pdf

        if gen_type == 'homo':
            pdf_values = np.random.uniform(1, 1, len(classes))
            for i in range(magic_value):
                for x, scaled_pdf in zip(classes, pdf_values):
                    ranged_values[int(x)] += scaled_pdf

        clients_data.append(ranged_values)
        return ranged_values

    def batch_data_upd(self, data_shard: Tuple[List[List[float]], List[List[int]]]) -> tf.data.Dataset:
        """Takes in a clients data shard and create a tfds object off it
        args:
            shard: a data, label constituting a client's data shard
            bs:batch size
        return:
            tfds object"""
        data, label = zip(*data_shard)
        dataset = tf.data.Dataset.from_tensor_slices((list(data), list(label)))
        return dataset.shuffle(len(label)).batch(self.batch_size)

    def read_data(self) -> Union[Tuple[tf.data.Dataset, tf.data.Dataset, tf.data.Dataset, List[int], List[int]], tf.data.Dataset]:
        """
         Read and return data. This is wrapper function to generate data based on it's name (MNIST, DAISEE)
        """
        if self.data_name == 'DAISEE':
            return self.read_full_daisee_data()
        elif self.data_name == 'MNIST':
            return self.read_full_mnist_data()
        else:
            return self.read_full_credit_scoring_data()

    def read_batched_data(self, NUM_CLASSES: int, user_id: int=None, clients_data_updated: tf.data.Dataset=None, ) -> tf.data.Dataset:
        """
         Read and return data. This is wrapper function to generate data based on it's name (MNIST, DAISEE)
         
         :param NUM_CLASSES: The number of classes to read.
         :param user_id: The user_id of the user who made the request.
         :param clients_data_updated: If not None the list of clients that have been updated
        """
        if self.data_name == 'DAISEE':
            return self.read_batched_daisee_data(user_id)

        elif self.data_name == 'MNIST':
            return self.read_batched_mnist_data(clients_data_updated, NUM_CLASSES)


    def get_data_size(self) -> int:
        if self.data_name == 'DAISEE':
            return self.get_daisee_dataset_size()
        elif self.data_name == 'MNIST':
            return self.get_mnist_dataset_size()

    

    def take_batch_of_data(self, data_to_work_on: Union[tf.data.Dataset, np.ndarray], percentage_to_train_on: float=0.1) -> Tuple[List[tf.data.Dataset], List[tf.data.Dataset]]:
        """
        Takes a batch of data with balanced class representation.

        Args:
            data: The input data, either as a list of tuples or a TensorFlow dataset.
            percentage_to_train_on: The percentage of data to use for training (0.0 to 1.0).

        Returns:
            A tuple containing two TensorFlow datasets: the training batch and remaining data.
        """
        remain_data = []
        X_train = []
        y_train = []
        clients_all_labels = []
        selected_indices = []
        if isinstance(data_to_work_on, tf.data.Dataset):
            for la in list(data_to_work_on):
                for data in la[0]:
                    X_train.append(data.numpy())
                for label in la[1]:
                    if len(label.numpy().shape)>1:
                      y_train.append(np.argmax(label.numpy()))
                    else:
                      y_train.append(label.numpy())


        else:
            X_train = [da[0] for da in data_to_work_on]
            if len(data_to_work_on[0][1])>1:
              y_train = [np.argmax(da[1]) for da in data_to_work_on]
            else:
              y_train = [da[1] for da in data_to_work_on]

        classes = np.unique(y_train)

        pdf_values = np.random.uniform(1, 1, len(classes))
        ranged_values = [0 for _ in range(len(classes))]
        for i in range((len(list(data_to_work_on))//2)):
            for x, scaled_pdf in zip(classes, pdf_values):
                ranged_values[int(x)] += scaled_pdf
        ranged_values = [percentage_to_train_on*i for i in ranged_values]
        
        ss = [np.where(np.array(y_train) == j)[0] for j in range(len(ranged_values))]
        qq = [len(i) for i in ss]
        muni = min(qq)

        for i in range(len(ranged_values)):
          if ranged_values[i] > muni:
            ranged_values[i] = muni

        for idx, label in enumerate(ranged_values):
          client_label_zero = []
          i = 0
          while i < ranged_values[idx]:
              if ss[idx][i] not in selected_indices:
                  client_label_zero.append(ss[idx][i])
                  i += 1
              else:
                  i += 1
                  ranged_values[idx] += 1
                  continue
          clients_all_labels.append(client_label_zero)
        all_data_indicies = []
        all_data_indicies.extend(clients_all_labels[0])
        all_data_indicies.extend(clients_all_labels[1])
        
        new_X_train = np.array(X_train)[all_data_indicies]
        new_y_train = np.array(y_train)[all_data_indicies]

        mask = np.where(~np.isin(np.arange(len(y_train)), all_data_indicies))[0]
        remain_X_train = np.array(X_train)[mask]
        remain_y_train = np.array(y_train)[mask]
        dataset = tf.data.Dataset.from_tensor_slices((new_X_train, new_y_train)).batch(self.batch_size)
        remian_dataset = tf.data.Dataset.from_tensor_slices((remain_X_train, remain_y_train)).batch(self.batch_size)

        return dataset, remian_dataset
