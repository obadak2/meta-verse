import tensorflow as tf
from typing import Tuple, Option, Any, List
import cv2
from skimage.transform import resize
import random
import imgaug.augmenters as iaa



class ImagePreprocessor:
  """
  This class provides various image preprocessing functions for emotion recognition tasks.
  """

  def __init__(self, target_size:Tuple[int, int]=(224, 224), grayscale:bool=False, normalization_method:str='minmax'):
    """
    Args:
        target_size: A tuple representing the desired output image size (width, height).
        grayscale: A boolean flag indicating whether to convert images to grayscale.
        normalization_method: The normalization method to use. Options include:
            - 'minmax': Scales pixel values between 0 and 1.
            - 'standardization': Subtracts the mean and divides by the standard deviation.
    """
    self.target_size = target_size
    self.grayscale = grayscale
    self.normalization_method = normalization_method
    self.path_to_haar = '/content/drive/MyDrive/meta-verse/pre trained models/'

  def resize(self, image:List[Any])->List[Any]:
    """
    Resizes an image to the target size using bilinear interpolation.

    Args:
        image: A NumPy array representing the image.

    Returns:
        A resized NumPy array representing the image.
    """
    return resize(image, self.target_size, mode='reflect', anti_aliasing=True)

  def normalize(self, image:List[Any])->List[Any]:
    """
    Normalizes pixel values based on the chosen normalization method.

    Args:
        image: A NumPy array representing the image.

    Returns:
        A normalized NumPy array representing the image.
    """
    if self.normalization_method == 'minmax':
      return (image - image.min()) / (image.max() - image.min())
    elif self.normalization_method == 'standardization':
      return (image - image.mean()) / image.std()
    else:
      raise ValueError(f"Unsupported normalization method: {self.normalization_method}")

  def augment(self, image:List[Any], horizontal_flip:bool=True, rotation_range:int=10, noise_factor:int=0)->List[Any]:
    """
    Augments an image with random flipping, rotation, and optional noise injection.

    Args:
        image: A NumPy array representing the image.
        horizontal_flip: A boolean flag indicating whether to apply random horizontal flip.
        rotation_range: The maximum rotation angle (in degrees) for random rotation.
        noise_factor: The amount of Gaussian noise to add (0 for no noise).

    Returns:
        An augmented NumPy array representing the image.
    """

    augmented_image = image

    if horizontal_flip and random.random() > 0.5:
      augmented_image = iaa.Fliplr(1.0)(image=augmented_image)

    if rotation_range > 0:
      augmented_image = iaa.Rotate((-rotation_range, rotation_range))(image=augmented_image)

    if noise_factor > 0:
      augmented_image = iaa.AdditiveGaussianNoise(loc=0, scale=(0, noise_factor))(image=augmented_image)

    return augmented_image

  def to_grayscale(self, image:List[Any])->List[Any]:
    """
    Converts an image to grayscale if grayscale conversion is enabled.

    Args:
        image: A NumPy array representing the image.

    Returns:
        A grayscale NumPy array representing the image (if grayscale is enabled),
        otherwise the original image.
    """
    if self.grayscale:
      return cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    else:
      return image

  def detect_and_crop_face(self, image:List[Any])->List[Any]:
    """
    (Optional) Detects and crops the face from the image using OpenCV's Haar cascade classifier.

    **Note:** This function requires OpenCV to be installed (`pip install opencv-python`).

    Args:
        image: A NumPy array representing the image.

    Returns:
        A cropped NumPy array representing the face region (if a face is detected),
        otherwise the original image.
    """
    face_cascade = cv2.CascadeClassifier(self.path_to_haar+'haarcascade_frontalface_default.xml')  # Replace with your Haar cascade path
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray, 1.1, 4)

    if len(faces) > 0:  # Check if at least one face is detected
      (x, y, w, h) = faces[0]  # Extract coordinates of the first detected face
      return image[(y+int(0.01*y)):y+h+int(0.01*(y+h)), x+int(0.01*(x)):x+w+int(0.01*(x+w))]  # Crop the image based on the face coordinates
    else:
      return image  # Return the original image if no face is detected
  def reduce_noise(self, image:List[Any], kernel_size:Tuple[int, int]=(3, 3))->List[Any]:
    """
    Reduces noise in an image using a Gaussian filter.

    Args:
        image: A NumPy array representing the image.
        kernel_size: A tuple representing the size of the Gaussian kernel (default is (3, 3)).

    Returns:
        A NumPy array representing the denoised image.
    """

    # Convert image to float32 for better performance
    # image = image.astype(np.float32)

    # Apply Gaussian filtering
    denoised_image = cv2.GaussianBlur(image, kernel_size, 0)

    # Convert back to original data type (usually uint8)
    return denoised_image