from sklearn.feature_selection import SelectFromModel
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_selection import RFE
from sklearn.linear_model import Lasso
import pandas as pd
import numpy as np

def non_selected_features(chunk):
    """
    return indexes of Nan columns as a list.
    """
    return list(np.where(np.isnan(chunk).any(axis=0))[0])

def selected_features(chunk):
    """
    returns the chunks after deleting the Nan columns 
    ** Nan columns are non-selected columns
    """
    
    return list(np.where(~np.isnan(chunk).any(axis=0))[0])

def selected_features_SelectBest(X_train, y_train,method_fs):
    sel = SelectFromModel(method_fs)
    sel.fit(X_train, y_train)
    selected_feat= X_train.columns[(sel.get_support(indices=True))]
    return selected_feat


def non_selected_features_SelectBest(X_train, y_train,method_fs):
    sel = SelectFromModel(method_fs)
    sel.fit(X_train, y_train)
    selected_feat= X_train.columns[(sel.get_support())]
    return ([i for i in np.arange(0, len(X_train.columns)) if i not in selected_feat])
    

def fill_na(chunk):
    """
    returns the chunk after fillingthe Nan values of it with -1.
    """
    if not isinstance(chunk, pd.DataFrame):
      chunk = pd.DataFrame(chunk)
    chunk.fillna(-1, inplace=True)
    return chunk

def chunk_non_selected_features_SelectBest(X_train, y_train,method_fs):
    sel = SelectFromModel(method_fs)
    sel.fit(X_train, y_train)
    selected_feat= list(X_train.columns[(sel.get_support(indices=True))])
    return np.array(X_train)[ :, selected_feat]

def chunk_without_non_selected_features(chunk):
    """
    returns the chunks after deleting the Nan columns
    ** Nan columns are non-selected columns
    """
    return chunk[ :,~np.isnan(chunk).any(axis=0)]

def select_best_features_RFE(X_train, y_train, n_features,step):
    """
    Select the best 'n_features' features from the input 'X_train' data
    using Recursive Feature Elimination (RFE) with a logistic regression
    estimator. Returns a numpy array containing only the selected features.
    """
    estimator = LogisticRegression(solver='lbfgs', max_iter=100)
    selector = RFE(estimator, n_features_to_select=n_features,step=step)
    selector.fit(X_train, y_train)
    selected_features = X_train.columns[selector.support_].tolist()
    return selected_features

def non_select_best_features_RFE(X_train, y_train, n_features,step):
    """
    Select the best 'n_features' features from the input 'X_train' data
    using Recursive Feature Elimination (RFE) with a logistic regression
    estimator. Returns a numpy array containing only the selected features.
    """
    estimator = LogisticRegression(solver='lbfgs', max_iter=100)
    selector = RFE(estimator, n_features_to_select=n_features,step=step)
    selector.fit(X_train, y_train)
    selected_features = X_train.columns[selector.support_].tolist()
    return ([i for i in np.arange(0, len(X_train.columns)) if i not in selected_features])

def chunk_select_best_features_RFE(X_train, y_train, n_features,step):
    """
    Select the best 'n_features' features from the input 'X_train' data
    using Recursive Feature Elimination (RFE) with a logistic regression
    estimator. Returns a numpy array containing only the selected features.
    """
    estimator = LogisticRegression(solver='lbfgs', max_iter=100)
    selector = RFE(estimator, n_features_to_select=n_features,step=step)
    selector.fit(X_train, y_train)
    selected_features = X_train.columns[selector.support_].tolist()
    # return np.array(X_train)[ :, selected_features]
    return selected_features

class CustomLabelBinirizer:
    def __init__(self, negative_label:int=0, positive_label:int=1):
        """
        """
        self.negative_label = negative_label
        self.positive_label = positive_label
        self.transformer = None
        self.is_fit = False
        
    def fit(self, y):
        """
        """
        if self.is_fit:return
        if type(y) is list:
            y = np.array(y)
        ##############
        y_unique = np.unique(y)
        # print("what the bug {}".format(len(y_unique)))
        y_unique.sort()
        _shape = len(y_unique)
        ##############
        self.transformer = np.zeros((_shape, _shape))
        for unique_value in y_unique:
            self.transformer[unique_value, unique_value] = 1
        self.is_fit = True
        return 
    
    def transform(self, y):
        """
        """
        if not self.is_fit:
            self.fit(y)
        y_bin = []
        for value in y:
            y_bin.extend(self.transformer[value, :].tolist())
        return np.array(y_bin)
            
        
    def fit_transform(self, y):
        self.fit(y)
        return self.transform(y)
    
    def inverse_transform(self, y_bin):
        return np.argmax(y_bin, axis=1)