from Codes.Data.InductionForData import InductionForData
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.preprocessing import LabelBinarizer
from sklearn.preprocessing import OneHotEncoder
from sklearn.linear_model import Lasso
from Codes.Data.PreProcessing import *
from tqdm.notebook import tqdm as tq
from collections import Counter
from scipy.stats import norm
import tensorflow as tf
import pandas as pd
import numpy as np
import pickle
import random
import copy
import os
from typing import List, Tuple, Optional, Union


class CreditScoringDataInduction(InductionForData):

    def __init__(self, dataset_path: str,
                 batch_size: int,
                 data_name: str,
                 number_of_users: int,
                 non_iid_strength_factor: int,
                 sigma: float,
                 classes: List[int],
                 feature_selection_method: str,
                 apply_feature_selection: str
                 ):
        super(DaiseeDataInduction).__init__()
        self.apply_feature_selection = apply_feature_selection
        self.feature_selection_method = feature_selection_method
        self.dataset_path = dataset_path
        self.batch_size = batch_size
        self.data_name = data_name
        self.number_of_users = number_of_users
        self.non_iid_strength_factor = non_iid_strength_factor
        self.sigma = sigma
        self.classes = classes
        self.selected_indices = []

    
    def read_full_credit_scoring_data(self) -> Tuple[tf.data.Dataset, tf.data.Dataset, tf.data.Dataset, List[int], List[int]]:
        """
         Read credit scoring data and return it as a dictionary. This is a no - op if there is no data
        """
        train_raw = pd.read_csv(self.dataset_path)
        labels = train_raw[train_raw.columns[-1]]
        data = train_raw.drop(train_raw.columns[-1], axis=1)
        X_train, X_test, y_train, y_test = train_test_split(data, labels, test_size = 0.3, stratify=labels)
        X_train, X_b, y_train, y_b = train_test_split(X_train, y_train, test_size = 0.1, stratify=y_train)

        # images = []

        lb = LabelBinarizer()
        label_list = lb.fit_transform(y_train)
        y_test = lb.fit_transform(y_test)
        y_b = lb.fit_transform(y_b)
        
        # onehot_encoder = OneHotEncoder(sparse=False)
        # label_list = onehot_encoder.fit_transform(label_list.reshape(-1, 1))
        # y_test = onehot_encoder.fit_transform(y_test.reshape(-1, 1))

        X_train = X_train.reset_index()
        X_test = X_test.reset_index()
        X_b = X_b.reset_index()
        clients_data_upda = []
        clients_data_upda_test = []
        boosting_data = []

        if self.apply_feature_selection:
          # print(X_train.shape)
          non_selected_features, selected_features = self.preprocess_clients_data_fs(X_b, y_b)
          if self.feature_selection_method == 'single_agent':
              if self.data_name.split('_')[0]=='faoselm':
                print('hh')
                X_train = fill_na(X_train)
                X_b = fill_na(X_b)
                X_test = fill_na(X_test)
          else:
              if not self.data_name.split('_')[0]=='faoselm':
                X_train = X_train[selected_features]
                X_test = X_test[selected_features]
                X_b = X_b[selected_features]

          for i in range(len(X_train)):
              clients_data_upda.append((X_train.loc[i].values, label_list[i]))
          
        else:
          for i in range(len(X_train)):
              clients_data_upda.append((X_train.loc[i].values, label_list[i]))

        test_dataset = tf.data.Dataset.from_tensor_slices((X_test.values, y_test)).batch(self.batch_size)
        boosting_dataset = tf.data.Dataset.from_tensor_slices((X_b.values, y_b)).batch(self.batch_size)

        return clients_data_upda, test_dataset, boosting_dataset, selected_features, non_selected_features

    def read_batched_credit_scoring_data(self, user_id: int, data_length: int) -> List[int]:
        return [user_id[0]+1] if user_id[0]<data_length else [user_id[0]]


    def retrieve_data_from_indices(self, images: List[Union[List[float], float]], labels: List[int], indices: List[int]) -> tf.data.Dataset:
        """
         Retrieve data from indices.
         
         :param images: A list of images to be used for retrieval.
         :param labels: A list of labels to be used for retrieval.
         :param indices: A list of indices to be retrieved from the images
        """
        flattened_data_images = []
        flattened_data_labels = []
        
        for i in range(len(indices)):
            flattened_data_images.append(np.array(images)[indices[i]])
            flattened_data_labels.append(np.array(labels)[indices[i]])


        flattened_data_images = [item for sublist in flattened_data_images for item in sublist]
        flattened_data_labels = [item for sublist in flattened_data_labels for item in sublist]


        flattened_data_images = np.array(flattened_data_images)
        if len(flattened_data_images.shape)==1:
          flattened_data_images = np.expand_dims(flattened_data_images, axis=0)

        flattened_data_labels = np.array(flattened_data_labels)
        if len(flattened_data_labels.shape)==1:
          flattened_data_labels = np.expand_dims(flattened_data_labels, axis=0)

        dataset = tf.data.Dataset.from_tensor_slices((flattened_data_images, flattened_data_labels))

        return dataset.shuffle(len(flattened_data_labels)).batch(self.batch_size)

  