from Codes.Data.InductionForData import InductionForData
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.preprocessing import LabelBinarizer
from sklearn.preprocessing import OneHotEncoder
from sklearn.linear_model import Lasso
from Codes.Data.PreProcessing import *
from tqdm.notebook import tqdm as tq
from collections import Counter
from scipy.stats import norm
import tensorflow as tf
import pandas as pd
import numpy as np
import pickle
import random
import copy
import os
from typing import List, Tuple, Optional, Union


class MnistDataInduction(InductionForData):

    def __init__(self, dataset_path: str,
                 batch_size: int,
                 data_name: str,
                 number_of_users: int,
                 non_iid_strength_factor: int,
                 sigma: float,
                 classes: List[int],
                 feature_selection_method: str,
                 apply_feature_selection: str
                 ):
        super(MnistDataInduction).__init__()
        self.apply_feature_selection = apply_feature_selection
        self.feature_selection_method = feature_selection_method
        self.dataset_path = dataset_path
        self.batch_size = batch_size
        self.data_name = data_name
        self.number_of_users = number_of_users
        self.non_iid_strength_factor = non_iid_strength_factor
        self.sigma = sigma
        self.classes = classes
        self.selected_indices = []

    
    def read_full_mnist_data(self) -> Tuple[List[float], List[int]]:
        """
         Read MNIST data and return it as a dictionary. This is a no - op if there is no data
        """
        train_raw = pd.read_csv(self.dataset_path)
        labels = train_raw[train_raw.columns[-1]]
        data = train_raw.drop(train_raw.columns[-1], axis=1)
        images = []

        for i in range(len(data.values)):
            images.append(data.values[i].reshape(28, 28).flatten())

        lb = LabelBinarizer()
        label_list = lb.fit_transform(labels)

        clients_data_upda = []
        for i in range(len(images)):
            clients_data_upda.append((images[i], label_list[i]))
        return clients_data_upda


    def read_batched_mnist_data(self, clients_data_updated: List[Tuple[List[float], List[int]]], NUM_CLASSES: int) -> List[tf.data.Dataset]:
        """
         Read MNIST data and split it to clients with a level of heterogenity.
         
         :param clients_data_updated: A list of client ids to update
         :param NUM_CLASSES: The number of classes to
        """
        # NUM_CLASSES = len(np.unique(np.array(y_train)))
        clients_batched_upd = dict()

        clients_data = sorted(clients_data_updated, key=lambda x: int(np.argmax(x[1])))

        # Then, we need to separate the samples by creating a dictionary of 10 different keys (0 -> 9) which are the labels.
        # Each key has a value which is a list of all samples belong to the label that the key represents.
        sorted_by_label = {str(k): [] for k in range(10)}
        for sample in clients_data:
            sorted_by_label[str(int(np.argmax(sample[1])))].append(sample)

        # This dictionary is going to hold the dataset separated by clients.
        # The dictionary will be filled in the for loop below where we distribute the samples between the clients.
        clients_datasets_dict = {k: [] for k in range(self.number_of_users)}

        # The following dictionary contains 10 different keys (0 -> 9) which are the labels.
        # Each value is a list of size equals to self.number_of_users.
        # Each list contains random picks along a range of "label_list" length (indices in ascending order). Some of theses indices will be dropped depening on the non_iid_strength_factor as we will see in the loop below.
        # This means that each client "i" will have chunk of samples from label_list[i-1] to label_list[i] for each label.
        lables_indices = {k: sorted(random.sample(range(len(label_list)), (self.number_of_users - 1))) for
                          label, label_list, k in
                          zip(sorted_by_label.keys(), sorted_by_label.values(), range(NUM_CLASSES))}

        # The dictionary here is meant to track the next available label index for the next client.
        index_tracker = {label: 0 for label, _ in sorted_by_label.items()}

        # Starting the distributing process by first looping over the clients.
        for i in range(self.number_of_users):
            # client_execluded_classes holds the labels that the i-th client will be deprived from
            client_execluded_classes = random.sample(range(NUM_CLASSES), self.non_iid_strength_factor)
            for label, label_list in sorted_by_label.items():
                # If the outer loop is not at the last client:
                if i != (self.number_of_users - 1):
                    # Pop the next index whatever.
                    label_index = lables_indices[int(label)].pop(0)
                    # If the i-th client can't have the current label in the inner loop, continue.
                    if int(label) in client_execluded_classes:
                        continue
                    # If the outer loop is at the last client, give me the last index.
                    else:
                        label_index = len(label_list)
                # Assign the next chunck to the i-th client.
                clients_datasets_dict[i].extend(label_list[index_tracker[label]:label_index])
                random.shuffle(clients_datasets_dict[i])
                index_tracker[label] = label_index

        for (client_name, data) in tq(clients_datasets_dict.items()):
            clients_batched_upd[client_name] = self.batch_data_upd(data, NUM_CLASSES)

        return clients_batched_upd
      
    def read_batched_equal_split_mnist_data(self, clients_data_upda: List[tf.data.Dataset]=None) -> List[tf.data.Dataset]:
        """
        Read MNIST data for equal split. 

        :param clients_data_upda: Upda data from client
        """

        llabels = [lab[1] for lab in clients_data_upda]
        ddata = [lab[0] for lab in clients_data_upda]
        new_order_client = {}
        classes = np.unique(np.argmax(llabels, axis=1))
        first_classes = [cl for cl in classes[:(len(classes) // 2)]]
        second_classes = [cl for cl in classes[(len(classes) // 2):]]

        taken_classes = {label: -1 for label in classes}
        for i in range(self.number_of_users):
            dataa2 = []
            labels2 = []
            for j, q in zip(range(len(first_classes)), range(len(second_classes))):
                if taken_classes[first_classes[j]] == -1 and taken_classes[second_classes[q]] == -1:
                    indices = np.where(first_classes[j] == np.argmax(llabels, axis=1))[0]
                    indices2 = np.where(second_classes[q] == np.argmax(llabels, axis=1))[0]

                    dataa2.append([ddata[im] for im in indices])
                    dataa2.append([ddata[dm] for dm in indices2])
                    labels2.append([llabels[im] for im in indices])
                    labels2.append([llabels[dm] for dm in indices2])
                    dataset = tf.data.Dataset.from_tensor_slices(
                        (np.concatenate((np.array(dataa2[0]), np.array(dataa2[1])), axis=0),
                         np.concatenate((np.array(labels2[0]), np.array(labels2[1])), axis=0))).batch(self.batch_size)

                    new_order_client[i] = dataset
                    taken_classes[j] = 0
                    taken_classes[q] = 0
                    break
                else:
                    continue
        return new_order_client

    def get_mnist_image(self, X_test: np.ndarray[np.ndarray], y_test: np.ndarray[np.ndarray]) -> Tuple[np.ndarray, int]:
        """
         Returns MNIST image.

         :param X_test: numpy array of shape ( n_samples n_features )
         :param y_test: numpy array of shape ( n_samples
        """
        rand_idx = random.randrange(len(X_test))
        random_image = X_test[rand_idx]
        random_label = y_test[rand_idx]
        img = random_image.reshape(28, 28)
        return img, random_label

    def get_mnist_dataset_size(self) -> int:
        train_raw = pd.read_csv(self.dataset_path)
        return len(train_raw)

    def prepare_test_data(self, test_dataset_path: str) -> tf.data.Dataset:
        train_raw = pd.read_csv(test_dataset_path)
        labels = train_raw["label"]
        data = train_raw.drop("label", axis=1)
        images = []

        for i in range(len(data.values)):
            images.append(data.values[i].reshape(28, 28).flatten())

        lb = LabelBinarizer()
        label_list = lb.fit_transform(labels)

        return super.batch_data_upd(list(zip(images, label_list)))

    def retrieve_data_from_indices(self, images: List[Union[List[float], float]], labels: List[int], indices: List[int]) -> tf.data.Dataset:
        """
         Retrieve data from indices.
         
         :param images: A list of images to be used for retrieval.
         :param labels: A list of labels to be used for retrieval.
         :param indices: A list of indices to be retrieved from the images
        """
        flattened_data_images = []
        flattened_data_labels = []
        
        for i in range(len(indices)):
            flattened_data_images.append(np.array(images)[indices[i]])
            flattened_data_labels.append(np.array(labels)[indices[i]])


        flattened_data_images = [item for sublist in flattened_data_images for item in sublist]
        flattened_data_labels = [item for sublist in flattened_data_labels for item in sublist]

        return super.batch_data_upd(list(zip(flattened_data_images, flattened_data_labels)))

  