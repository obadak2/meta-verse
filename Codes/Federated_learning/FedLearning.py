from sklearn.metrics import precision_score, recall_score, f1_score, accuracy_score
from Codes.Inter_Clustering_Grouping.ICG import CustomizedClustering
from Codes.Differential_privacy.Diff_privacy import DP
from Codes.Client_Server.Client import Client
from Codes.Client_Server.Server import Server
from multiprocessing import Pool
from tqdm.notebook import tqdm
from typing import List, Tuple, Optional, Any, Union
import keras.backend as K
import tensorflow as tf
import numpy as np
import datetime
import copy


class FL:
    def __init__(self, number_of_clients: int, comm_rounds: int, metrics: List[str], loss_function: str,
                 optimizer: str, server: Server, clipping_norm: float, epsilon: float, user_per_round: int,
                 learning_rate: float, momentum: float, non_iid_strength_factor: int, sigma: float, classes: List[int], magic_value: int, binarizer: str) -> None:
        self.comm_rounds = comm_rounds
        self.binarizer = binarizer
        self.metrics = metrics
        self.clipping_norm = clipping_norm
        self.epsilon = epsilon
        self.user_per_round = user_per_round
        self.learning_rate = learning_rate
        self.momentum = momentum
        self.loss_function = loss_function
        self.optimizer = optimizer
        self.server = server
        self.number_of_clients = number_of_clients
        self.non_iid_strength_factor = non_iid_strength_factor
        self.sigma = sigma
        self.classes = classes
        self.magic_value = magic_value
        self.data_size = 0
        self.def_privacy = None
        self.clients = {}
        self.clients_batched = {}
        self.test_data = []
        self.global_model_favg = None
        self.data_size = 0
        self.icg_instance = None
        self.beta = 2
        self.alpha = 0.5
        self.clients_groups = None

    def growth_function(self, func_type: str, current_round: int, num_of_groups: int=2, the_order: str='increase') -> float:
        """
         Produce the cluster number based on function type user will choose (increase, monotic, fixed value) and the iteration round
         
         :param func_type: Type of the growth function.
         :param current_round: Iteration round
         :param num_of_groups: Number of groups to be used in the growth function.
         :param the_order: Order of the growth function which is used with the function type (increase).
         :returns: The value of the growth function in the given round
        """
        if func_type == 'monotic':
            return self.beta * (self.alpha * np.log(current_round) + 1)
        elif func_type == 'fixed':
            return 4
        else:
            if the_order == 'increase':
                num_of_groups += 1
                return num_of_groups
            else:
                num_of_groups -= 1
                return num_of_groups

    def prepare_fl(self,
                   dataset_path: str='None',
                   test_dataset_path: str='None',
                   batch_size: int=1,
                   data_name: str='None',
                   input_shape: Tuple[int, int]=None,
                   number_of_classes: int=2,
                   gen_type: str='None',
                   the_allowed_hete: int=2,
                   feature_selection_method: str='None',
                   apply_feature_selection: bool=False,
                   magic_value: int=500,
                   act: int=2,
                   start: int=0,
                   end: int=0
                   ) -> Tuple[dict[int, tf.data.Dataset], dict[int, tf.data.Dataset], List[int], List[int]]:
        """
        Prepares the data and the clients to be used with the Federated System 

        :param dataset_path: Path to the dataset to be used for training.
        :param test_dataset_path: Path to the test dataset to be used for testing.
        :param batch_size: Batch size to use for training.
        :param data_name: Name of the data to be used for training.
        :param input_shape: Shape of the input to the dataset.
        :param number_of_classes: Number of classes in the dataset.
        :param gen_type: Type of the data generation to be used.
        :param the_allowed_hete: Maximum number of heterogenity for class to be considered this is used with (extreme heterogeneity).
        :returns: A ready to use FluGrain object that can be used to train and test
        """
        client = Client(dataset_path,
                        batch_size,
                        data_name,
                        input_shape,
                        self.optimizer,
                        self.loss_function,
                        self.metrics,
                        self.number_of_clients,
                        number_of_classes,
                        self.non_iid_strength_factor,
                        self.sigma,
                        self.classes,
                        feature_selection_method,
                        apply_feature_selection,
                        self.binarizer)
        if data_name.split('_')[-1]=='MNIST':
          clients_data_updated = client.read_full_data()
          self.test_data = client.prepare_test_data(test_dataset_path)

        elif data_name.split('_')[-1]=='DAISEE':
          clients_data_updated, self.test_data = client.read_full_data()
          val_data, self.test_data = client.prepare_test_data(test_data=self.test_data)

        else:
          clients_data_updated, self.test_data, boosting_dataset, selected_features, non_selected_features = client.read_full_data()

        copied_array = copy.deepcopy(self.classes)

        label_list = [label[1] for label in clients_data_updated]
        images = [label[0] for label in clients_data_updated]
        q = 0
        start_time =  datetime.datetime.now()
        with tqdm(total=self.number_of_clients, desc="Creating Clients") as progress_bar:
          for i in range(0, self.number_of_clients):
              client_start_time =  datetime.datetime.now()  # Track individual client start time
              if gen_type == 'extreme':
                  magic_value = np.sum(label_list, axis=0)[q] // act
              if data_name.split('_')[-1]=='DAISEE':
                mu = 0
              if data_name.split('_')[-1]=='MNIST':
                mu = np.random.randint(0, len(copied_array))
              self.clients[i] = Client(dataset_path,
                                      batch_size,
                                      data_name,
                                      input_shape,
                                      self.optimizer,
                                      self.loss_function,
                                      self.metrics,
                                      self.number_of_clients,
                                      number_of_classes,
                                      self.non_iid_strength_factor,
                                      self.sigma,
                                      self.classes,
                                      feature_selection_method,
                                      apply_feature_selection,
                                      self.binarizer)
                                      
              self.clients_batched[i] = self.clients[i].read_batched_data(images=images,
                                                                          magic_value=magic_value,
                                                                          split_type=gen_type,
                                                                          mu=mu,
                                                                          label_list=label_list,
                                                                          user_id=[i],
                                                                          clients_data_updated=clients_data_updated,
                                                                          data_length=len(clients_data_updated)-1,
                                                                          )
              if i % act == 0:
                  start = end
                  end = end + the_allowed_hete
                  q = q + 1
              client_time =  datetime.datetime.now() - client_start_time
              progress_bar.set_postfix({"creating client time is:":client_time.total_seconds()})
              progress_bar.update()
        overall_time =  datetime.datetime.now() - start_time
        print(f"Overall Client Creation Time: {overall_time.total_seconds():.2f}s")

        self.data_size = client.get_data_size()
        self.def_privacy = DP(self.number_of_clients,
                              self.clipping_norm,
                              self.epsilon,
                              self.comm_rounds,
                              self.user_per_round,
                              self.data_size,
                              self.learning_rate,
                              self.momentum)
        if data_name.split('_')[-1]=='MNIST':
            return self.clients_batched, self.test_data
        elif data_name.split('_')[-1]=='DAISEE':
            return self.clients_batched, self.test_data, val_data
        else:
            return self.clients_batched, boosting_dataset, selected_features, non_selected_features

    def weight_scaling_factor(self, clients_batched, client_id: int) -> float:
        """
         Weightscalling factor for batch_in. The weightscalling factor is the ratio of the number of local and global clients that have at least one sample in the batch.

         :param clients_batched: A dictionary of client_ids as keys and a list of numpy arrays as values.
         :param client_id: The id of the client to calculate the weightscalling factor for.
         :returns: The weightscalling factor for the batch_in with id client_id as keys and a float
        """
        client_names = list(clients_batched.keys())
        bs = list(clients_batched[client_id])[0][0].shape[0]
        global_count = sum([tf.data.experimental.cardinality(clients_batched[client_name]).numpy() for client_name in
                            client_names]) * bs
        local_count = tf.data.experimental.cardinality(clients_batched[client_id]).numpy() * bs
        return local_count / global_count

    def scale_model_weights(self, weight: List[np.ndarray], scalar: float) -> List[np.ndarray]:
        """
         Scale weights by a scalar. This is used to scale the weights when training a neural network.

         :param weight: List of weights in the form [ x y z ]
         :param scalar: Scalar to scale the weights by. Default is 1.
         :returns: List of weights scaled by scalar ( same length as weight ). Note that the order of weights is preserved
        """
        weight_final = []
        steps = len(weight)
        # Add a weight to the final weight
        for i in range(steps):
            weight_final.append(scalar * weight[i])
        return weight_final

    def sum_scaled_weights(self, scaled_weight_list: List[List[np.ndarray]]) -> List[np.ndarray]:
        """
         Sums scaled weights across all layers. This is useful for computing the mean of a list of scaled weights across all layers.

         :param scaled_weight_list: List of lists of scaled weights. Each list is a list of layer weights. Each layer weight is a 2D NumPy array where the first dimension is the batch size
         :returns: List of averaged weights across all layers. The length of the list is the same as the length of the scaled
        """
        avg_grad = list()
        # Computes average gradient of all weights.
        for grad_list_tuple in zip(*scaled_weight_list):
            layer_mean = tf.math.reduce_sum(grad_list_tuple, axis=0)
            avg_grad.append(layer_mean)
        return avg_grad

    def data_generator(self, dataset: dict[int, tf.data.Dataset]) -> Tuple[np.ndarray, np.ndarray]:
      for x, y in dataset:
        x_array = x.numpy()
        y_array = y.numpy()
        return x_array[0], y_array[0]

    def calculate_metrics(self, y_pred: List | np.ndarray, y_true: List | np.ndarray, classes: int=2) -> Tuple[float, float, float, float]:
      if not isinstance(y_pred, np.ndarray):
        y_pred = np.array(y_pred)
        
      if not isinstance(y_true, np.ndarray):
        y_true = np.array(y_true)
      
      if len(y_pred.shape)==1:
        if classes==2:
          precision = precision_score([np.argmax(i) for i in y_true], y_pred)
          recall = recall_score([np.argmax(i) for i in y_true], y_pred)
          f1 = f1_score([np.argmax(i) for i in y_true], y_pred)
          score = accuracy_score([np.argmax(i) for i in y_true], y_pred)
        else:
          precision = precision_score([np.argmax(i) for i in y_true], y_pred, average='weighted')
          recall = recall_score([np.argmax(i) for i in y_true], y_pred, average='weighted')
          f1 = f1_score([np.argmax(i) for i in y_true], y_pred, average='weighted')
          score = accuracy_score([np.argmax(i) for i in y_true], y_pred)

      else:
        if classes==2:
          precision = precision_score([np.argmax(i) for i in y_true], 
          [np.argmax(j) for j in y_pred])
          recall = recall_score([np.argmax(i) for i in y_true], 
          [np.argmax(j) for j in y_pred])
          f1 = f1_score([np.argmax(i) for i in y_true], 
          [np.argmax(j) for j in y_pred])
          score = accuracy_score([np.argmax(i) for i in y_true], 
          [np.argmax(j) for j in y_pred])
        else:
          precision = precision_score([np.argmax(i) for i in y_true], 
          [np.argmax(j) for j in y_pred], average='weighted')
          recall = recall_score([np.argmax(i) for i in y_true], 
          [np.argmax(j) for j in y_pred], average='weighted')
          f1 = f1_score([np.argmax(i) for i in y_true], 
          [np.argmax(j) for j in y_pred], average='weighted')
          score = accuracy_score([np.argmax(i) for i in y_true], 
          [np.argmax(j) for j in y_pred])
      return score, recall, precision, f1

    def train_model(self, 
                    data=None,
                    data_name:str='None',
                    model=None,
                    metrics:str='accuracy',
                    loss_function:str='loss',
                    epochs:int=1,
                    non_selected_features:list=None,
                    selected_features:list=None) -> Union[Tuple[tf.keras.Model, tf.keras.callbacks.History], tf.keras.Model]:
        if data_name.split('_')[0] == 'mlp':
            his, model = self.server.compile_fit_model(model,
                                                    data,
                                                    metrics,
                                                    loss_function,
                                                    epochs=epochs,
                                                    non_selected_features=non_selected_features,
                                                    selected_features=selected_features,
                                                    )
            return his, model
        else:
            model = self.server.compile_fit_model(model,
                                                    data,
                                                    metrics,
                                                    loss_function,
                                                    non_selected_features=non_selected_features,
                                                    selected_features=selected_features,
                                                    )
            return model
      
    def predict_model(self, data=None, model=None) -> Tuple[tf.keras.Model, np.ndarray, float, float, float, float]:
        X_data = []
        y_labels = []
        for element in list(X_data):
          for data in element[0]:
              data.append(data.numpy())
          for label in element[1]:
              y_labels.append(label.numpy())
              
        prediction = model.predict(X_data)
        score, recall, precision, f1 = self.calculate_metrics(prediction, y_labels, len(self.classes))

        return model, prediction, score, recall, precision, f1