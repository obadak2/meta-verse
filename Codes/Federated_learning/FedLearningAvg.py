from sklearn.metrics import precision_score, recall_score, f1_score, accuracy_score
from Codes.Inter_Clustering_Grouping.ICG import CustomizedClustering
from Codes.Differential_privacy.Diff_privacy import DP
from Codes.Client_Server.Client import Client
from Codes.Client_Server.Server import Server
from multiprocessing import Pool
from tqdm.notebook import tqdm
from typing import List, Tuple, Optional, Any, Union
import keras.backend as K
import tensorflow as tf
import numpy as np
import datetime
import copy


class FLAvg:
    def __init__(self, number_of_clients: int, comm_rounds: int, metrics: List[str], loss_function: str,
                 optimizer: str, server: Server, clipping_norm: float, epsilon: float, user_per_round: int,
                 learning_rate: float, momentum: float, non_iid_strength_factor: int, sigma: float, classes: List[int], magic_value: int, binarizer: str) -> None:
        super(FLAvg).__init__()
        self.comm_rounds = comm_rounds
        self.binarizer = binarizer
        self.metrics = metrics
        self.clipping_norm = clipping_norm
        self.epsilon = epsilon
        self.user_per_round = user_per_round
        self.learning_rate = learning_rate
        self.momentum = momentum
        self.loss_function = loss_function
        self.optimizer = optimizer
        self.server = server
        self.number_of_clients = number_of_clients
        self.non_iid_strength_factor = non_iid_strength_factor
        self.sigma = sigma
        self.classes = classes
        self.magic_value = magic_value
        self.data_size = 0
        self.def_privacy = None
        self.clients = {}
        self.clients_batched = {}
        self.test_data = []
        self.global_model_favg = None
        self.icg_instance = None
        self.beta = 2
        self.alpha = 0.5
        self.clients_groups = None

    def run_fed_avg(self,
                    batch_size: int=None,
                    data_name: str='None',
                    input_shape: Tuple[int, int]=None,
                    number_of_classes: int=None,
                    dp: bool=False,
                    icg_bool: bool=False,
                    seq: bool=False,
                    pretrain: bool=False,
                    boosting_dataset: list=None,
                    selected_features: list=None,
                    non_selected_features: list=None,
                    metric_used: str = 'accuracy',
                    loss_used: str = 'loss',
                    growth_function_type='monotic'
                    ) -> Optional[Tuple[dict[str, float | np.ndarray],tf.keras.Model | Any, dict[str, np.ndarray | int]]] :
        """
            Runs federated average system. It is used to calculate the performance of the model and return the results in a dictionary
            
            :param batch_size: Number of examples in the dataset ( default : 10 )
            :param data_name: Name of the data to be used for training
            :param input_shape: Shape of the input data ( default : tuple ( int int ))
            :param number_of_classes: Number of classes in the dataset ( default : int ( 10 ))
            :param dp: Boolean if True use DP ( default : False )
            :param icg_bool: Boolean if True use clustering ( default : False )
            :param seq: Boolean if True use seqential training ( default : False )
            :param metric_used: String metric used ( default :'accuracy')
            :param loss_used: String loss used to determine loss ( default :'loss')
            :param growth_function_type: String growth function type ( default : monotic )
            :returns: Dictionary with keys : train_results : List of training
        """
        if dp:
            clipping_norm = self.def_privacy.get_clipping_norm()
            c = self.def_privacy.get_c_value()
            epsilon = self.def_privacy.get_epsilon()
        fl_accuracy_loss_model = {}
        groups_over_rounds = {}
        test_data_data = []
        test_data_label = []
        self.global_model_favg = self.server.build_model()
        if pretrain:
            print('Training the servers model on 10% of the data')
            data_used_to_train_global_model, boosting_dataset = self.clients[1].take_batch_of_data(boosting_dataset)
            self.global_model_favg = super.train_model(data_used_to_train_global_model,
                                                      self.global_model_favg,
                                                      self.metrics,
                                                      self.loss_function,
                                                      epochs=5,
                                                      non_selected_features=non_selected_features,
                                                      selected_features=selected_features)
            data_used_to_train_local_models, boosting_dataset = self.clients[1].take_batch_of_data(boosting_dataset)
            prediction, self.global_model_favg, score, recall, precision, f1 = super.predict_model(data_used_to_train_local_models, self.global_model_favg)
            
            print(f'the pretrained model recall:{recall}')
            print(f'the pretrained model precision:{precision}')
            print(f'the pretrained model f1:{f1}')
            print(f'the pretrained model accuracy:{score}')

            print('done\n\n\n')

        if icg_bool:
            client_names = list(self.clients_batched.keys())
            for comm_round in range(1, self.comm_rounds + 1):
                global_weights = self.global_model_favg.get_weights()

                print(f"\t__Round__:{comm_round}")
                scaled_local_weight_list = list()

                number_of_groups = super.growth_function(growth_function_type, comm_round)
                keys = list(self.clients_batched.keys())
                V = []

                for key in keys:
                    label_for_each_user = []
                    for la in list(self.clients_batched[key]):
                        for label in la[1]:
                            label_for_each_user.append(label.numpy())
                    V.append(np.sum([label_nes for label_nes in label_for_each_user], axis=0))
                V = np.array(V)
                print('number of groups', number_of_groups)
                clustering = CustomizedClustering(number_of_clients=self.number_of_clients,
                                                  number_of_clusters=int(number_of_groups),
                                                  clients_data=V)
                self.clients_groups = clustering.fit()

                scaled_cluster_weight_list = list()
                groups_over_rounds[f'groups {number_of_groups}'] = self.clients_groups
                groups_over_rounds[f'round'] = comm_round
                for group_id, group in enumerate(self.clients_groups):
                    cluster_model = self.clients[group_id + 1].build_model()
                    cluster_model.set_weights(global_weights)
                    data_size_for_cluster = 0
                    global_count = 0
                    for idx in group:
                        his, self.global_model_favg = super.train_model(self.clients_batched[idx + 1],
                                                                          cluster_model,
                                                                          self.metrics,
                                                                          self.loss_function,
                                                                          epochs=1)
                        his, self.global_model_favg = super.train_model(data_used_to_train_local_models,
                                                                          cluster_model,
                                                                          self.metrics,
                                                                          self.loss_function,
                                                                          epochs=1)
                        fl_accuracy_loss_model[f'client {idx + 1}' + metric_used + str(comm_round)] = his.history[metric_used]
                        fl_accuracy_loss_model[f'client {idx + 1}' + loss_used + str(comm_round)] = his.history[loss_used]
                        fl_accuracy_loss_model[f'client {idx + 1}' + ' round ' + str(comm_round)] = comm_round
                        
                        data_size_for_client = list(self.clients_batched[idx + 1])[0][0].shape[0] * len(
                            list(self.clients_batched[idx + 1]))

                        data_size_for_cluster += data_size_for_client
                        cluster_weights = cluster_model.get_weights()

                        if dp:
                            print('started the training with DP')
                            sensitivity = 2 * (clipping_norm) / (self.data_size / self.number_of_clients)
                            stddev = (c * sensitivity * (comm_round + 1)) / epsilon
                            new_cluster_weights = []
                            for index, weight in enumerate(cluster_weights):
                                cluster_weights[index] = self.def_privacy.clip_weights(cluster_weights[index])
                                stddev = tf.cast(stddev, cluster_weights[index].dtype)
                                noise = tf.random.normal(shape=tf.shape(cluster_weights[index]), mean=0.0,
                                                         stddev=stddev)
                                cluster_weights[index] = cluster_weights[index] + noise

                    global_count = sum([tf.data.experimental.cardinality(self.clients_batched[client_name]).numpy() for client_name in
                         list(self.clients_batched.keys())]) * batch_size

                    print(data_size_for_cluster, global_count)
                    scaling_factor = data_size_for_cluster / global_count
                    scaled_weights = super.scale_model_weights(cluster_weights, scaling_factor)
                    scaled_cluster_weight_list.append(scaled_weights)
                    K.clear_session()
                    average_weights = super.sum_scaled_weights(scaled_cluster_weight_list)

                self.global_model_favg.set_weights(average_weights)
                self.global_model_favg.compile(loss=self.loss_function,
                                               optimizer='Adam',
                                               metrics=self.metrics)
                prediction, self.global_model_favg, score, recall, precision, f1 = super.predict_model(self.test_data, self.global_model_favg)
                evaluate = self.global_model_favg.evaluate(self.test_data)
                fl_accuracy_loss_model['global model' + ' accuracy ' + str(comm_round)] = score
                fl_accuracy_loss_model['global model' + ' loss ' + str(comm_round)] = evaluate[0]
                fl_accuracy_loss_model['global model' + ' precision ' + str(comm_round)] = precision
                fl_accuracy_loss_model['global model' + ' recall ' + str(comm_round)] = recall
                fl_accuracy_loss_model['global model' + ' f1_score ' + str(comm_round)] = f1
            return fl_accuracy_loss_model, self.global_model_favg, groups_over_rounds

        else:
            if seq:
              start_time =  datetime.datetime.now()
              with tqdm(total=self.comm_rounds, desc="iterating through communication rounds") as progress_bar:
                for comm_round in range(self.comm_rounds):
                    comm_start_time =  datetime.datetime.now()
                    if data_name.split('_')[0] == 'mlp':
                        for idx in range(1, self.number_of_clients + 1):
                            his, self.global_model_favg = super.train_model(self.clients_batched[idx],
                                                                          self.global_model_favg,
                                                                          self.metrics,
                                                                          self.loss_function, epochs=1)
                            fl_accuracy_loss_model[f'client {idx}' + metric_used + str(comm_round)] = his.history[metric_used]
                            fl_accuracy_loss_model[f'client {idx}' + loss_used + str(comm_round)] = his.history[loss_used]
                            fl_accuracy_loss_model[f'client {idx}' + ' round ' + str(comm_round)] = comm_round
                    else:
                      with tqdm(total=self.number_of_clients, desc="iterating through clients") as clients_progress_bar:
                        for idx in range(self.number_of_clients):
                            x, y = self.data_generator(self.clients_batched[idx])
                            noise_range = 0.00001
                            noise = np.random.uniform(-noise_range/2, noise_range/2, np.array(x).shape)
                            x = np.repeat(x[np.newaxis, ...] + noise, 50, axis=0)
                            noise = np.random.uniform(-noise_range/2, noise_range/2, np.array(y).shape)
                            y = np.repeat(y[np.newaxis, ...], 50, axis=0)
                            trainset = tf.data.Dataset.from_tensor_slices((x, y)).batch(self.batch_size)

                            self.global_model_favg = super.train_model(trainset,
                                                      self.global_model_favg,
                                                      non_selected_features=non_selected_features,
                                                      selected_features=selected_features)
                            self.global_model_favg = super.train_model(boosting_dataset,
                                                      self.global_model_favg,
                                                      non_selected_features=non_selected_features,
                                                      selected_features=selected_features)
                            clients_progress_bar.set_postfix({f"client {idx+1}"})
                            clients_progress_bar.update()


                    prediction, self.global_model_favg, score, recall, precision, f1 = super.predict_model(self.test_data, self.global_model_favg)
                    real_test_data_label = []
                    for element in list(self.test_data):
                      for label in element[1]:
                          real_test_data_label.append(label.numpy())
                    fl_accuracy_loss_model['global model' + ' accuracy ' + str(comm_round)] = score
                    fl_accuracy_loss_model['global model' + ' precision ' + str(comm_round)] = precision
                    fl_accuracy_loss_model['global model' + ' recall ' + str(comm_round)] = recall
                    fl_accuracy_loss_model['global model' + ' f1_score ' + str(comm_round)] = f1
                    fl_accuracy_loss_model['global model' + ' y_pred ' + str(comm_round)] = [np.argmax(j) for j in prediction]
                    fl_accuracy_loss_model['global model' + ' y_true ' + str(comm_round)] = [np.argmax(i) for i in real_test_data_label]
                    domm_round_time =  datetime.datetime.now() - comm_start_time
                    progress_bar.set_postfix({f"round {comm_round+1} Time": f"{domm_round_time.total_seconds()}s"})
                    print(f"accuracy for global model at rounds {comm_round}: {score}")
                    print(f"f1 score for global model at rounds {comm_round}: {f1}")                    
                    progress_bar.update()
                overall_time =  datetime.datetime.now() - start_time
                print(f"Overall Training Time: {overall_time.total_seconds():.2f}s")

                return fl_accuracy_loss_model, self.global_model_favg
            else:
                for comm_round in range(self.comm_rounds):
                    print(f"\t__Round__:{comm_round}")
                    global_weights = self.global_model_favg.get_weights()

                    scaled_local_weight_list = list()

                    for idx in range(1, self.number_of_clients + 1):
                        local_model = self.clients[idx].build_model()
                        local_model.set_weights(global_weights)
                        his = local_model.fit(self.clients_batched[idx], epochs=1, verbose=1)

                        fl_accuracy_loss_model[f'client {idx}' + metric_used + str(comm_round)] = his.history[
                            metric_used]
                        fl_accuracy_loss_model[f'client {idx}' + loss_used + str(comm_round)] = his.history[loss_used]
                        fl_accuracy_loss_model[f'client {idx}' + ' round ' + str(comm_round)] = comm_round
                        local_weights = local_model.get_weights()

                        if dp:
                            print('started the training with DP')
                            sensitivity = 2 * (clipping_norm) / (self.data_size / self.number_of_clients)
                            stddev = (c * sensitivity * (comm_round + 1)) / epsilon
                            new_local_weights = []

                            for index, weight in enumerate(local_weights):
                                local_weights[index] = self.def_privacy.clip_weights(local_weights[index])
                                stddev = tf.cast(stddev, local_weights[index].dtype)
                                noise = tf.random.normal(shape=tf.shape(local_weights[index]), mean=0.0, stddev=stddev)
                                local_weights[index] = local_weights[index] + noise

                        scaling_factor = super.weight_scaling_factor(self.clients_batched, idx)
                        scaled_weights = super.scale_model_weights(local_weights, scaling_factor)
                        scaled_local_weight_list.append(scaled_weights)
                        K.clear_session()
                        average_weights = super.sum_scaled_weights(scaled_local_weight_list)

                    self.global_model_favg.set_weights(average_weights)
                    self.global_model_favg.compile(loss=self.loss_function,
                                                   optimizer='Adam',
                                                   metrics=self.metrics)

                    prediction, self.global_model_favg, score, recall, precision, f1 = super.predict_model(self.test_data, self.global_model_favg)
                    evaluate = self.global_model_favg.evaluate(self.test_data)
                    fl_accuracy_loss_model['global model' + ' accuracy ' + str(comm_round)] = score
                    fl_accuracy_loss_model['global model' + ' loss ' + str(comm_round)] = evaluate[0]
                    fl_accuracy_loss_model['global model' + ' precision ' + str(comm_round)] = precision
                    fl_accuracy_loss_model['global model' + ' recall ' + str(comm_round)] = recall
                    fl_accuracy_loss_model['global model' + ' f1_score ' + str(comm_round)] = f1

                return fl_accuracy_loss_model, self.global_model_favg