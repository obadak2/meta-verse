from sklearn.metrics import precision_score, recall_score, f1_score, accuracy_score
from Codes.Inter_Clustering_Grouping.ICG import CustomizedClustering
from Codes.Utils.losses import local_contrastive_loss, global_contrastive_loss
from Codes.Differential_privacy.Diff_privacy import DP
from Codes.Client_Server.Client import Client
from Codes.Client_Server.Server import Server
from Codes.Federated_learning.FedLearning import FL
from multiprocessing import Pool
from tqdm.notebook import tqdm
from typing import List, Tuple, Optional, Any, Union
import keras.backend as K
import tensorflow as tf
import numpy as np
import datetime
import pickle
import copy
import os


class FedPcl(FL):
    def __init__(self, number_of_clients: int, comm_rounds: int, metrics: List[str], loss_function: str,
                 optimizer: str, server: Server, clipping_norm: float, epsilon: float, user_per_round: int,
                 learning_rate: float, momentum: float, non_iid_strength_factor: int, sigma: float, classes: List[int], magic_value: int, binarizer: str) -> None:
        super().__init__(number_of_clients,
                    comm_rounds,
                    metrics,
                    loss_function,
                    optimizer,
                    server,
                    clipping_norm,
                    epsilon,
                    user_per_round,
                    learning_rate,
                    momentum,
                    non_iid_strength_factor,
                    sigma,
                    classes,
                    magic_value,
                    binarizer
                    )
        self.comm_rounds = comm_rounds
        self.binarizer = binarizer
        self.metrics = metrics
        self.clipping_norm = clipping_norm
        self.epsilon = epsilon
        self.user_per_round = user_per_round
        self.learning_rate = learning_rate
        self.momentum = momentum
        self.loss_function = loss_function
        self.optimizer = optimizer
        self.server = server
        self.number_of_clients = number_of_clients
        self.non_iid_strength_factor = non_iid_strength_factor
        self.sigma = sigma
        self.classes = classes
        self.magic_value = magic_value
        self.data_size = 0
        self.def_privacy = None
        self.clients = {}
        self.clients_batched = {}
        self.test_data = []
        self.global_model_favg = None
        self.data_size = 0
        self.icg_instance = None
        self.beta = 2
        self.alpha = 0.5
        self.clients_groups = None


    def local_proto_aggregation(self, protos, client_data):
      """
      Average the protos for each local user
      """
      agg_protos = {}
      labels = []
      for step, (x_batch_train, y_batch_train) in enumerate(client_data):
        labels.append(y_batch_train.numpy())

      class_counts = np.sum(labels[0], axis=0)
      for label, proto_list in protos.items():
          proto = tf.stack(proto_list)
          agg_protos[int(label)] = proto * 1/class_counts[label]
      return agg_protos

    def server_proto_aggregation(self, local_protos_list, clients_batched):
        agg_protos_label = dict()
        clients_own_labels = {}
        clients_labels_counts = []
        for j in clients_batched.keys():
          client_labels_counts = []
          for step, (x_batch_train, y_batch_train) in enumerate(clients_batched[j]):
            client_labels_counts.append(y_batch_train.numpy())
          clients_labels_counts.append(client_labels_counts)

        clients_labels_counts = [np.sum(clients_labels_counts[0][i], axis=0) for i in range(len(clients_labels_counts))]
        
        for client_id in range(len(clients_labels_counts)):
          for i in range(len(clients_labels_counts[client_id])):
            if clients_labels_counts[client_id][i]>0:
              if i in clients_own_labels:
                clients_own_labels[i].append((client_id, np.sum(clients_labels_counts[client_id])))
              else:
                clients_own_labels[i] = [(client_id, np.sum(clients_labels_counts[client_id]))]

        for idx in range(len(local_protos_list)):
            local_protos = local_protos_list[idx]
            for label in local_protos.keys():
                if label in agg_protos_label:
                    agg_protos_label[label].append(local_protos[label])
                else:
                    agg_protos_label[label] = [local_protos[label]]

        for label in agg_protos_label.keys():
          total_data_lengths = sum([client[1] for client in clients_own_labels[label]])
          summation_over_label = np.sum(clients_labels_counts, axis=0)[label]
          agg_protos_label[label] = np.sum(agg_protos_label[label], axis=0) * (total_data_lengths / summation_over_label)
          agg_protos_label[label] = (1/len(clients_own_labels[label])) * agg_protos_label[label]

        return agg_protos_label

    def customized_training_loop(self, 
            model,
            feature_extraction_model,
            client_data,
            server_protos,
            all_clients_protos,
            temp,
            optimizer,
            epochs,
            batch_size,
            number_of_users,
            ):
        """
        Function to perform Federated Prototypical Contrastive Learning (FedPCL) training.

        Args:
            model: Keras model to be trained.
            feature_extraction_model: Keras model for feature extraction.
            client_data: Iterator providing client data.
            server_protos: Prototypes from the server.
            all_clients_protos: Prototypes from all clients.
            temp: Temperature parameter for contrastive loss.
            optimizer: Keras optimizer instance.
            epochs: Number of training epochs.
            batch_size: Batch size for training.

        Returns:
            trained_model_weights: Model weights after training.
            epoch_losses: Dictionary containing losses for each epoch.
        """

        # Metric for training accuracy
        train_acc_metric = tf.keras.metrics.CategoricalAccuracy()
        # train_acc_metric = tf.keras.metrics.SparseCategoricalAccuracy()

        epoch_losses = {'total': [], 'global': [], 'local': []}
        start_time =  datetime.datetime.now()

        with tqdm(total=epochs, desc="epochs") as progress_bar:
          for epoch in range(epochs):
              epoch_start_time =  datetime.datetime.now()  # Track individual client start time

              epoch_loss = {'total': [], 'global': [], 'local': []}

              for step, (x_batch_train, y_batch_train) in enumerate(client_data):
                  progress_bar.set_postfix({"work for batch": step})

                  batch_loss = {'global': [], 'local': [], 'total': []}

                  reps = feature_extraction_model.predict(x_batch_train, verbose=0)
                  with tf.GradientTape() as tape:
                      features, logits = model(reps, training=True)

                      # Calculate individual losses
                      if len(all_clients_protos) == number_of_users:
                        global_loss_value = global_contrastive_loss(server_protos, temp)(y_batch_train, features)
                        local_loss_value = local_contrastive_loss(all_clients_protos, temp)(y_batch_train, features)

                        # Combine the losses
                      else:
                        other_loss_fun = tf.keras.losses.CategoricalCrossentropy(from_logits=True)

                        global_loss_value = other_loss_fun(logits, y_batch_train)
                        local_loss_value = 0

                      total_loss_value = global_loss_value + local_loss_value
                      # Append individual losses for logging purposes
                      batch_loss['global'].append(global_loss_value)
                      batch_loss['local'].append(local_loss_value)
                      batch_loss['total'].append(total_loss_value)

                  # Calculate gradients and update model parameters
                  grads = tape.gradient(total_loss_value, model.trainable_weights)
                  optimizer.apply_gradients(zip(grads, model.trainable_weights))

                  # Update training metric
                  train_acc_metric.update_state(y_batch_train, logits)


                  # train_acc_metric.update_state(np.argmax(y_batch_train.numpy(), axis=1),
                  #                               np.argmax(logits.numpy(), axis=1))

                  # Aggregate losses for the epoch
                  for key in batch_loss.keys():
                      epoch_loss[key].append(tf.reduce_mean(batch_loss[key]))

              # Calculate mean losses for the epoch
              for key in epoch_loss.keys():
                  epoch_loss[key] = tf.reduce_mean(epoch_loss[key])
                  epoch_losses[key].append(epoch_loss[key])

              # Display metrics at the end of each epoch
              train_acc = train_acc_metric.result()
              print(f"Training acc over epoch {epoch + 1}: {float(train_acc):.4f}")

              # Reset training metric at the end of each epoch
              train_acc_metric.reset_states()
              epoch_time =  datetime.datetime.now() - epoch_start_time
              progress_bar.set_postfix({"Loss": epoch_loss['total'].numpy(), "Accuracy": train_acc.numpy()})
              progress_bar.update()


        # Get the trained model weights
        trained_model_weights = model.get_weights()

        agg_protos_label = {}
        for x_batch_train, label_g in client_data:
            reps = feature_extraction_model.predict(x_batch_train, verbose=0)
            features, _ = model(reps, training=False)
            for i in range(len(label_g)):
                label = label_g[i].numpy()
                label = np.argmax(label)
                if label in agg_protos_label:
                    agg_protos_label[label].append(features[i])
                else:
                    agg_protos_label[label] = [features[i]]

        # Aggregate prototypes
        for label, proto_list in agg_protos_label.items():
            agg_protos_label[label] = tf.reduce_mean(tf.stack(proto_list), axis=0)

        return trained_model_weights, epoch_losses, agg_protos_label


    def fed_pcl(self,
                batch_size: int=None,
                data_name: str='None',
                input_shape: Tuple[int, int]=None,
                number_of_classes: int=None,
                dp: bool=False,
                icg_bool: bool=False,
                seq: bool=False,
                pretrain: bool=False,
                boosting_dataset: list=None,
                selected_features: list=None,
                non_selected_features: list=None,
                metric_used: str = 'accuracy',
                loss_used: str = 'loss',
                growth_function_type='monotic',
                feature_extraction_model:tf.keras.Model=None,
                path_to_save_global_weights_over_rounds: str='empty',
                path_to_save_reults: str='empty',
                val_data: list=None,
                temp:float=0.07):
        """
            Runs federated average system. It is used to calculate the performance of the model and return the results in a dictionary
            
            :param batch_size: Number of examples in the dataset ( default : 10 )
            :param data_name: Name of the data to be used for training
            :param input_shape: Shape of the input data ( default : tuple ( int int ))
            :param number_of_classes: Number of classes in the dataset ( default : int ( 10 ))
            :param dp: Boolean if True use DP ( default : False )
            :param icg_bool: Boolean if True use clustering ( default : False )
            :param seq: Boolean if True use seqential training ( default : False )
            :param metric_used: String metric used ( default :'accuracy')
            :param loss_used: String loss used to determine loss ( default :'loss')
            :param growth_function_type: String growth function type ( default : monotic )
            :returns: Dictionary with keys : train_results : List of training
        """

      
        all_clients_protos = {}
        server_protos = {}
        fl_accuracy_loss_model = {}
        local_protos_before_agg = {}
        self.global_model_favg = self.server.build_model(feature_extraction_model)
        global_weights = self.global_model_favg.get_weights()
        start_time =  datetime.datetime.now()
        val_images, val_labels = zip(*val_data)
        val_images = np.array(val_images)
        val_labels = np.array(val_labels)

        # Starting point for the communication rounds
        with tqdm(total=self.comm_rounds, desc="iterating through communication rounds") as progress_bar:
          for comm_round in range(self.comm_rounds):
              if os.path.exists(f'{path_to_save_global_weights_over_rounds}/global_model_weights_at_{comm_round}.h5'):
                  self.global_model_favg.load_weights(f'{path_to_save_global_weights_over_rounds}/global_model_weights_at_{comm_round}.h5')
                  global_weights = self.global_model_favg.get_weights()
                  continue
              scaled_local_weight_list = list()
              local_weights, local_loss_global, local_loss_local, local_loss_total,  = [], [], [], []
              comm_start_time =  datetime.datetime.now()

              # Starting point for iterating over clients
              with tqdm(total=self.number_of_clients, desc="iterating through clients") as clients_progress_bar:
                for i in range(self.number_of_clients):
                  local_model = self.clients[i].build_model(feature_extraction_model)
                  local_model.set_weights(global_weights)

                  w, loss, protos = self.customized_training_loop(copy.deepcopy(local_model),
                                feature_extraction_model,
                                self.clients_batched[i],
                                server_protos,
                                all_clients_protos,
                                temp,
                                self.optimizer,
                                1,
                                batch_size,
                                self.number_of_clients
                                )
                  agg_protos = self.local_proto_aggregation(protos, self.clients_batched[i])
                  local_weights.append(copy.deepcopy(w))
                  local_loss_global.append(copy.deepcopy(loss['global']))
                  local_loss_local.append(copy.deepcopy(loss['local']))
                  local_loss_total.append(copy.deepcopy(loss['total']))
                  local_protos_before_agg[i] = copy.deepcopy(agg_protos)

                  scaling_factor = super().weight_scaling_factor(self.clients_batched, i)
                  scaled_weights = super().scale_model_weights(w, scaling_factor)
                  scaled_local_weight_list.append(scaled_weights)
                  # K.clear_session()
                  average_weights = super().sum_scaled_weights(scaled_local_weight_list)

                  fl_accuracy_loss_model[f'client {i}' + 'global_loss' + str(comm_round)] = loss['global']
                  fl_accuracy_loss_model[f'client {i}' + 'local_loss' + str(comm_round)] = loss['local']
                  fl_accuracy_loss_model[f'client {i}' + 'total_loss' + str(comm_round)] = loss['total']
                  clients_progress_bar.set_postfix({"client":f"{i+1}"})
                  clients_progress_bar.update()
              
              server_protos = self.server_proto_aggregation(local_protos_before_agg, self.clients_batched)
              all_clients_protos = copy.deepcopy(local_protos_before_agg)
              if comm_round % 20 == 0:
                for idx in range(self.number_of_clients):
                  loss, total, correct = 0.0, 0.0, 0.0
                  for x_batch_train, label_g in self.clients_batched[idx]:
                    reps = feature_extraction_model(x_batch_train)
                    props, features = self.global_model_favg(reps, training=False)
                    pred_labels = tf.cast(tf.argmax(props, axis=1), tf.int64)
                    y_true = tf.cast(tf.argmax(label_g, axis=1), tf.int64)
                    correct += tf.reduce_sum(tf.cast(tf.equal(pred_labels, y_true), dtype=tf.float32)).numpy()
                    total += len(label_g)                      
                    acc = correct / total

                    fl_accuracy_loss_model[f'client {idx}' + 'acc' + str(comm_round)] = acc
                    fl_accuracy_loss_model[f'client {idx}' + ' round ' + str(comm_round)] = comm_round
                    
              # Setting the local weights after aggregation to global model and saving the model
              # average_weights = super().sum_scaled_weights(local_weights)
              self.global_model_favg.set_weights(average_weights)
              # self.global_model_favg.save_weights(f'{path_to_save_global_weights_over_rounds}/global_model_weights_at_{comm_round}.h5')

              # Predicting using the global and calculate metrics
              reps = feature_extraction_model.predict(val_images)
              global_model_total, global_model_correct = 0.0, 0.0
              props, features = self.global_model_favg(reps, training=False)
              pred_labels = tf.cast(tf.argmax(props, axis=1), tf.int64)
              y_true = tf.cast(tf.argmax(val_labels, axis=1), tf.int64)
              global_model_correct += tf.reduce_sum(tf.cast(tf.equal(pred_labels, y_true), dtype=tf.float32)).numpy()
              global_model_total += len(val_labels)
              
              _, recall, precision, f1 = super().calculate_metrics(pred_labels, y_true, len(self.classes))
              acc = global_model_correct / global_model_total

              fl_accuracy_loss_model['global model' + ' accuracy ' + str(comm_round)] = acc
              fl_accuracy_loss_model['global model' + ' precision ' + str(comm_round)] = precision
              fl_accuracy_loss_model['global model' + ' recall ' + str(comm_round)] = recall
              fl_accuracy_loss_model['global model' + ' f1_score ' + str(comm_round)] = f1
              fl_accuracy_loss_model['global model' + ' y_pred ' + str(comm_round)] = [np.argmax(j) for j in pred_labels]
              fl_accuracy_loss_model['global model' + ' y_true ' + str(comm_round)] = [np.argmax(i) for i in y_true]
              a_file = open(
                    f"{path_to_save_reults}/fl_accuracy_loss_model_exp1.pkl",
                    "wb")
              pickle.dump(fl_accuracy_loss_model, a_file)
              a_file.close()
              domm_round_time =  datetime.datetime.now() - comm_start_time
              progress_bar.set_postfix({f"round {comm_round+1} Time": f"{domm_round_time.total_seconds()}s"})
              print(f"accuracy for global model at rounds {comm_round}: {acc}")
              print(f"f1 score for global model at rounds {comm_round}: {f1}")                    
              progress_bar.update()
          overall_time =  datetime.datetime.now() - start_time
          print(f"Overall Training Time: {overall_time.total_seconds():.2f}s")

          return fl_accuracy_loss_model, self.global_model_favg



